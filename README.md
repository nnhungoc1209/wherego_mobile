# WhereGo

## Overview

This project is a graduation essay thesis that I made when I was in university.

## Description

The purpose of this project is to develop a system that recommends interesting travel places and helps travelers book a hotel for their vacation.

In this project, I work in a 3 members group to develop a system, including a Front-end website, a Front-end mobile, and a Back-end API server. This is a Front-end mobile application that I take care of.

In this project, I implemented Provider as state management, MVVM as a design pattern, and Dio - a Flutter package - as a technique to make a request to the Back-end server. 

For more information about this system, please refer to this link:
- [Back-end server](https://gitlab.com/khanhnguyen3916/ct468_wherego)
- [Front-end website](https://gitlab.com/khanhnguyen3916/ct468_wherego_fe)
- [Front-end mobile](https://gitlab.com/khanhnguyen3916/ct468_wherego_mobile)

## Contact

If you have any questions or feedback about this project, please feel free to contact me via:
- [Facebook](https://www.facebook.com/nhungocc1212/)
- [Linkedin](https://www.linkedin.com/in/nhungoc-nguyen-1a969b233/)
- [GitHub](https://github.com/nnhungoc1209)
- [GitLab](https://gitlab.com/nnhungoc1209)

## Project image

![image](/uploads/0be1d5848c5aaadbfe857d47b82c0e41/image.png)

![image](/uploads/7d3eba294435ef1b2230260722b77f62/image.png)

![image](/uploads/f1763e961ff066d395ce9cec9e8fde26/image.png)

![image](/uploads/9f521ea139ae2e636e4f1e6942d155bb/image.png)

![image](/uploads/20fecadc7593be6e83f9df2a5b79b00d/image.png)
