class AppConstantApi {
  //Url
  static const baseUrl = 'http://192.168.1.14:8080/api/';

  //Account
  static const loginApiUrl = 'login?type=traveler';
  static const signupApiUrl = 'signup';

  //Traveler
  static const detailTravler = 'travelers/';
  static const changePassword = 'travelers/change-password';
  static const booking = 'bookings';

  //HotelReview
  static const hotelReview = 'hotels/';

  //PlaceReview
  static const placeReview = 'places/';

  //RestaurantReview
  static const restaurantReview = 'restaurants/';

  //Render
  static const renderImgUrl = 'render/';

  //Random
  static const randomHotel = 'hotels/random?quantity=6';
  static const randomPlace = 'places/random?quantity=6';
  static const randomRestaurant = 'restaurants/random?quantity=6';

  //Detail
  static const detailHotel = 'hotels/';
  static const detailPlace = 'places/';
  static const detailRestaurant = 'restaurants/';
  static const detailArticle = 'articles/';

  //Article
  static const getAllArticles = 'articles';
}
