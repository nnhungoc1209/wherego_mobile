class AppConstantRoute {
  //Account
  static const signUp = '/signup';
  static const login = '/login';

  static const profile = '/profile';
  static const booking = '/booking';
  static const changePassword = '/change_password';
  static const favorite = '/favorite';

  //Categories
  static const hotels = '/hotels_page';
  static const restaurants = '/restaurants_page';
  static const places = '/places_page';
  static const articles = '/articles_page';

  //Tabbar
  static const mainpage = '/main_page';
  static const search = '/search_page';
  static const account = '/account_page';

  //Search
  static const searchWithInput = '/search_with_input_page';

  //Detail
  static const hotelDetail = '/hotel_detail';
  static const placeDetail = '/place_detail';
  static const restaurantDetail = '/restaurant_detail';
}