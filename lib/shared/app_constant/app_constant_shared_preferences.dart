class AppConstantSharedPreferences {
  static const isLogin = 'is_login';
  static const sessionEmail = 'session_email';
  static const sessionPassword = 'session_password';
  static const sessionUsername = 'session_username';
  static const sessionTimeLogin = 'session_time_login';
  static const sessionJWTToken = 'session_jwt_token';
}