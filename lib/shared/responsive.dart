import 'package:flutter/material.dart';

class Responsive {

  static const designWidth = 375;
  static const designHeight = 812;

  double screenWidth = 0;
  double screenHeight = 0;

  Responsive(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    screenWidth = queryData.size.width;    //423.5294196844927
    screenHeight = queryData.size.height;  //705.0980524006648
  }

  getWidthResponsive(double width) {
    return (width / designWidth) * screenWidth;
  }

  getHeightResponsive(double height) {
    return (height / designHeight) * screenHeight;
  }

  getOtherSize(double size) {
    if (screenWidth < screenHeight) {
      return (size / designWidth) * screenWidth;
    }
    return (size / designWidth) * screenHeight;
  }
}