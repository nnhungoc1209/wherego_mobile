import 'package:flutter/material.dart';

class AppColors {
  static const Color bluePrimaryFalse = Color(0x084d88);
  static const Color bluePrimary = Color(0xFF084d88);
  static const Color blue1 = Color.fromRGBO(57, 113, 160, 0.5);
  static const Color blueButton = Color.fromRGBO(101, 200, 212, 0.8);

  static const Color whiteText = Color(0xffE5E5E5);
  static const Color white = Color(0xFFFFFFFF);
  static const Color white03 = Color.fromRGBO(1, 1, 1, 0.3);
  static const Color white0 = Color.fromRGBO(1, 1, 1, 0);

  static const Color grey = Color(0xff808080);
  static const Color darkGrey = Color(0xff565656);
  static const Color lightGrey = Color(0xffCFCFCF);

  static const Color black = Color(0xFF000000);
  static const Color black01 = Color.fromRGBO(0, 0, 0, 0.1);
  static const Color black06 = Color.fromRGBO(0, 0, 0, 0.6);

  static const Color errorRed = Color(0xFFD32F2F);
  static const Color favoriteRed = Color(0xFFf5054f);

  static const Color yellow = Color(0xFFFBB80F);
}