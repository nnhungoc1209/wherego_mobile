import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:where_go/view_models/article/get_all_articles_view_model.dart';
import 'package:where_go/view_models/article/get_article_detail_view_model.dart';
import 'package:where_go/view_models/traveler/favorite/favorite_provider.dart';
import 'package:where_go/view_models/traveler/profile/detail_traveler_view_model.dart';
import 'package:where_go/view_models/detail/detail_hotel_view_model.dart';
import 'package:where_go/view_models/detail/detail_place_view_model.dart';
import 'package:where_go/view_models/detail/detail_restaurant_view_model.dart';
import 'package:where_go/view_models/random/random_hotel_view_model.dart';
import 'package:where_go/view_models/random/random_place_view_model.dart';
import 'package:where_go/view_models/random/random_restaurant_view_model.dart';

class ProviderCustom {
  List<SingleChildWidget> getListMultiProvider() {
    return [
      //Random
      ChangeNotifierProvider.value(value: RandomPlaceViewModel()),
      ChangeNotifierProvider.value(value: RandomRestaurantViewModel()),
      ChangeNotifierProvider.value(value: RandomHotelViewModel()),

      //Detail
      ChangeNotifierProvider(create: (_) => DetailHotelViewModel()),
      ChangeNotifierProvider(create: (_) => DetailPlaceViewModel()),
      ChangeNotifierProvider(create: (_) => DetailRestaurantViewModel()),

      //Profile
      ChangeNotifierProvider(create: (_) => DetailTravelerViewModel()),

      //Favorite
      ChangeNotifierProvider(create: (_) => FavoriteProvider()),
      
      //Article
      ChangeNotifierProvider(create: (_) => GetAllArticlesViewModel()),
      ChangeNotifierProvider(create: (_) => GetArticleDetailViewModel())
    ];
  }
}