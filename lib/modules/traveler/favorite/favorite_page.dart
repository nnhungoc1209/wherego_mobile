import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/modules/components/account/traveler_info_widget.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';
import 'package:where_go/view_models/traveler/profile/detail_traveler_view_model.dart';
import 'favorite_widget.dart';

class FavoritePage extends StatefulWidget {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  State<FavoritePage> createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {

  @override
  void initState() {
    // TODO: implement initState

    init();

    super.initState();
  }

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? username = prefs.getString(AppConstantSharedPreferences.sessionUsername);
    String? token = prefs.getString(AppConstantSharedPreferences.sessionJWTToken);

    await Provider.of<DetailTravelerViewModel>(context, listen: false).getTravelerDetail(username!, token!);
  }

  @override
  Widget build(BuildContext context) {

    final loadedTravelerModel = Provider.of<DetailTravelerViewModel>(context).detailTravelerModel;

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100.0,
        elevation: 0,
        backgroundColor: AppColors.white,
        foregroundColor: AppColors.black,
        shadowColor: AppColors.white,
        title: TravelerInfoWidget(
          avatar: loadedTravelerModel.avatar ?? 'default.png',
          username: loadedTravelerModel.username ?? '',
          name: loadedTravelerModel.name ?? '',
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: const [
              FavoriteWidget()
            ]
          ),
        ),
      ),
    );
  }
}
