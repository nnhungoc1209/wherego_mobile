import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:where_go/modules/hotel/detail/hotel_detail_page.dart';
import 'package:where_go/modules/place/detail/place_detail_page.dart';
import 'package:where_go/modules/restaurant/detail/restaurant_detail_page.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';
import 'package:where_go/view_models/traveler/favorite/favorite_provider.dart';

class FavoriteWidget extends StatelessWidget {
  const FavoriteWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var favoriteList = Provider.of<FavoriteProvider>(context).favoriteList;
    double screenHeight = MediaQuery.of(context).size.height;

    return Column(
      children: [
        const TextHeader(
          text: 'Your Favorite List',
          top: 12.0,
          bottom: 12.0,
          alignment: Alignment.center,
        ),
        favoriteList.isNotEmpty
            ?  SizedBox(
          height: screenHeight,
          child: ListView(
            children: favoriteList.map((item) => UnconstrainedBox(
              child: FavoriteItemWidget(
                id: item.id ?? 0,
                name: item.name ?? '',
                image: item.image ?? '',
                type: item.type ?? '',
              ),
            )).toList(),
          ),
        )
            :  const TextTitle(text: 'No favorite item!'),
      ],
    );
  }
}


class FavoriteItemWidget extends StatelessWidget {
  final int id;
  final String name;
  final String image;
  final String type;

  const FavoriteItemWidget({
    Key? key,
    required this.id,
    required this.name,
    required this.image,
    required this.type
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            type == 'hotel'
                ? MaterialPageRoute(builder: (_) => HotelDetailPage(hotelId: id))
                : type == 'place'
                      ? MaterialPageRoute(builder: (_) => PlaceDetailPage(placeId: id))
                      : MaterialPageRoute(builder: (_) => RestaurantDetailPage(restaurantId: id))
        );
      },
      child: Container(
        height: 120,
        width: screenWidth - 50.0,
        margin: const EdgeInsets.symmetric(
            vertical: 12.0
        ),
        decoration: const BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: AppColors.black
                )
            )
        ),
        child: ListTile(
          leading: SizedBox(
              height: 100,
              width: 100,
              child: Image.network(
                AppConstantApi.baseUrl + AppConstantApi.renderImgUrl + image,
                fit: BoxFit.cover,
              )
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextHeader(
                text: name,
                textColor: AppColors.black,
                bottom: 7.0,
              ),
              Container(
                width: 90.0,
                padding: const EdgeInsets.all(2.0),
                decoration: BoxDecoration(
                  border: Border.all(
                      width: 1.0
                  ),
                  borderRadius: const BorderRadius.all(
                      Radius.circular(5.0) //
                  ),
                ),
                child: TextTitle(
                  text: type == 'hotel' ? 'Khách sạn' : type == 'place' ? 'Địa điểm' : 'Nhà hàng',
                  textColor: AppColors.darkGrey,
                  fontWeight: FontWeight.normal,
                  alignment: Alignment.center,
                ),
              )
            ],
          )
          //subtitle: Text('1212')
        ),
      ),
    );
  }
}
