import 'dart:io';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/modules/components/account/traveler_info_widget.dart';
import 'package:where_go/modules/signup/avatar_input_widget.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';
import 'package:where_go/style/alert/error_alert.dart';
import 'package:where_go/style/alert/success_alert.dart';
import 'package:where_go/style/button/button.dart';
import 'package:where_go/style/text/text_title.dart';
import 'package:where_go/style/textfield/textfield_account.dart';
import 'package:where_go/view_models/traveler/profile/detail_traveler_view_model.dart';
import 'package:where_go/view_models/traveler/update_information/update_information_view_model.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  DateTime? dayOfBirth;
  final TextEditingController _txtNameCtrl = TextEditingController();
  final TextEditingController _txtTelCtrl = TextEditingController();
  File? _pickedImage;

  String? token = '';
  String? username = '';

  void _selectImage(File pickedImage) {
    _pickedImage = pickedImage;
  }

  @override
  void initState() {
    // TODO: implement initState

    init();

    super.initState();
  }

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = prefs.getString(AppConstantSharedPreferences.sessionUsername);
    token = prefs.getString(AppConstantSharedPreferences.sessionJWTToken);

    await Provider.of<DetailTravelerViewModel>(context, listen: false).getTravelerDetail(username!, token!);
  }

  @override
  Widget build(BuildContext context) {

    final loadedTravelerModel = Provider.of<DetailTravelerViewModel>(context).detailTravelerModel;

    setState(() {
      _txtNameCtrl.text = loadedTravelerModel.name ?? '';
      _txtTelCtrl.text = loadedTravelerModel.tel ?? '';
      dayOfBirth = DateTime.fromMillisecondsSinceEpoch(loadedTravelerModel.dob ?? 0);
    });

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100.0,
        elevation: 0,
        backgroundColor: AppColors.white,
        foregroundColor: AppColors.black,
        shadowColor: AppColors.white,
        title: TravelerInfoWidget(
          avatar: loadedTravelerModel.avatar ?? 'default.png',
          username: loadedTravelerModel.username ?? '',
          name: loadedTravelerModel.name ?? '',
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [

                AvatarInputWidget(onSelectImage: _selectImage),

                const TextTitle(
                  text: 'Name',
                  bottom: 5.0,
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                ),
                TextfieldAccount(
                  controller: _txtNameCtrl,
                  suffixIcon: Icons.edit,
                ),

                const TextTitle(
                  text: 'Date of birth',
                  top: 16.0,
                  bottom: 5.0,
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                ),

                GestureDetector(
                  onTap: _presentDatePicker,
                  child: Container(
                    height: 65.0,
                    padding: const EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: AppColors.grey,
                        width: 1.0
                      ),
                        borderRadius: BorderRadius.circular(15.0)
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(DateFormat.yMd().format(dayOfBirth!)),
                        const Icon(Icons.edit, color: AppColors.grey),
                      ],
                    ),
                  ),
                ),


                const TextTitle(
                  text: 'Phone',
                  top: 16.0,
                  bottom: 5.0,
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                ),
                TextfieldAccount(
                  controller: _txtTelCtrl,
                  suffixIcon: Icons.edit,
                ),

                Button(
                  top: 20.0,
                  bottom: 20.0,
                  width: double.infinity,
                  title: 'Update',
                  backgroundColor: AppColors.blue1,
                  textColor: AppColors.black,
                  radius: 25.00,
                  onPressed: updateInfo,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: dayOfBirth ?? DateTime.now(),
      firstDate: DateTime(1990),
      lastDate: DateTime.now(),
    )
        .then((pickedDate) {
      if(pickedDate == null) {
        return;
      }
      setState(() {
        dayOfBirth = pickedDate;
      });
    })
    ;
  }

  Future<void> updateInfo() async {

  var updateInformationViewModel = UpdateInformationViewModel();

  await updateInformationViewModel.updateInformation(token!, username!, _txtNameCtrl.text, _txtTelCtrl.text, dayOfBirth!, _pickedImage);

    if(updateInformationViewModel.error == '') {

      SuccessAlert().successAlert(context: context, message: 'Thông tin đã được cập nhật.');

      Future.delayed(const Duration(seconds: 3)).then((_) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (_) => const ProfilePage())
        );
      });

    } else {
      ErrorAlert().errorAlert(context: context, content: updateInformationViewModel.error);
    }

  }
}
