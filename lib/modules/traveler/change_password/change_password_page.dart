import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/modules/components/account/traveler_info_widget.dart';
import 'package:where_go/modules/components/tabbar/bottom_tabbar.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';
import 'package:where_go/style/alert/error_alert.dart';
import 'package:where_go/style/alert/success_alert.dart';
import 'package:where_go/style/button/button.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';
import 'package:where_go/style/textfield/textfield_account.dart';
import 'package:where_go/validation/account/account_validation.dart';
import 'package:where_go/view_models/traveler/profile/detail_traveler_view_model.dart';
import 'package:where_go/view_models/traveler/change_password/change_password_view_model.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  State<ChangePasswordPage> createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {

  String token = '';
  String email = '';

  String? _errorCurrentPasswordText;
  String? _errorNewPasswordText;
  String? _errorConfirmPasswordText;

  final TextEditingController _txtCurrentPasswordCtrl = TextEditingController();
  final TextEditingController _txtNewPasswordCtrl = TextEditingController();
  final TextEditingController _txtConfirmPasswordCtrl = TextEditingController();

  @override
  void initState() {

    _errorCurrentPasswordText = null;
    _errorNewPasswordText = null;
    _errorConfirmPasswordText = null;

    init();

    super.initState();
  }

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? username = prefs.getString(AppConstantSharedPreferences.sessionUsername);
    token = prefs.getString(AppConstantSharedPreferences.sessionJWTToken) ?? '';
    email = prefs.getString(AppConstantSharedPreferences.sessionEmail) ?? '';

    await Provider.of<DetailTravelerViewModel>(context, listen: false).getTravelerDetail(username!, token);
  }

  @override
  Widget build(BuildContext context) {

      final loadedTravelerModel = Provider.of<DetailTravelerViewModel>(context).detailTravelerModel;

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100.0,
        elevation: 0,
        backgroundColor: AppColors.white,
        foregroundColor: AppColors.black,
        shadowColor: AppColors.white,
        title: TravelerInfoWidget(
          avatar: loadedTravelerModel.avatar ?? 'default.png',
          username: loadedTravelerModel.username ?? '',
          name: loadedTravelerModel.name ?? '',
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                const TextHeader(
                  text: 'Change Password',
                  alignment: Alignment.topCenter,
                  textColor: AppColors.black,
                ),
                const TextTitle(
                  text: 'Current Password',
                  top: 16.0,
                  bottom: 5.0,
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                ),
                TextfieldAccount(
                  controller: _txtCurrentPasswordCtrl,
                  lable: 'Current Password',
                  obscure: true,
                  errorText: _errorCurrentPasswordText,
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                ),

                const TextTitle(
                  text: 'New Password',
                  top: 16.0,
                  bottom: 5.0,
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                ),
                TextfieldAccount(
                  controller: _txtNewPasswordCtrl,
                  lable: 'New Password',
                  obscure: true,
                  errorText: _errorNewPasswordText,
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                ),

                const TextTitle(
                  text: 'Confirm Password',
                  top: 16.0,
                  bottom: 5.0,
                  fontWeight: FontWeight.normal,
                  fontSize: 14.0,
                ),
                TextfieldAccount(
                  controller: _txtConfirmPasswordCtrl,
                  lable: 'Confirm Password',
                  obscure: true,
                  errorText: _errorConfirmPasswordText,
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                ),

                Button(
                  top: 20.0,
                  bottom: 20.0,
                  width: double.infinity,
                  title: 'Update Password',
                  backgroundColor: AppColors.blue1,
                  textColor: AppColors.black,
                  radius: 25.00,
                  onPressed: changePassword,
                ),
              ],
            ),
          )
        ),
      ),
    );
  }

  bool _validate() {

    bool isValid = true;

    setState(() {
      _errorCurrentPasswordText = AccountValidation.isNullTextfield(_txtCurrentPasswordCtrl.text, 'Current Password');
      _errorNewPasswordText = AccountValidation.isNullTextfield(_txtNewPasswordCtrl.text, 'New Password');
      _errorConfirmPasswordText = AccountValidation.isNullTextfield(_txtConfirmPasswordCtrl.text, 'Confirm Password');
    });

    if(_txtNewPasswordCtrl.text != _txtConfirmPasswordCtrl.text){
      setState(() {
        _errorConfirmPasswordText = 'Confirm password is not match';
      });
      isValid = false;
    }

    return isValid;
  }

  Future<void> changePassword() async{
    var changePasswordViewModel = ChangePasswordViewModel();

    if(_validate()) {
      await changePasswordViewModel.changePassword(
        token,
        email,
        _txtCurrentPasswordCtrl.text,
        _txtNewPasswordCtrl.text
      );

      if(changePasswordViewModel.error == '') {
        SuccessAlert().successAlert(context: context, message: 'Mật khẩu đã được cập nhật');

        Future.delayed(const Duration(seconds: 5)).then((_) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (_) => const BottomTabbar(currentTab: 0))
          );
        });

      } else {
          ErrorAlert().errorAlert(context: context, content: changePasswordViewModel.error);
      }
    }
  }
}
