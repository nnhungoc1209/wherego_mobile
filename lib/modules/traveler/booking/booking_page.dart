import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/modules/traveler/booking/booking_item.dart';
import 'package:where_go/modules/components/account/traveler_info_widget.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';
import 'package:where_go/style/button/button.dart';
import 'package:where_go/view_models/traveler/profile/detail_traveler_view_model.dart';

class BookingPage extends StatefulWidget {
  const BookingPage({Key? key}) : super(key: key);

  @override
  State<BookingPage> createState() => _BookingPageState();
}

class _BookingPageState extends State<BookingPage> {

  @override
  void initState() {
    // TODO: implement initState

    init();

    super.initState();
  }

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? username = prefs.getString(AppConstantSharedPreferences.sessionUsername);
    String? token = prefs.getString(AppConstantSharedPreferences.sessionJWTToken);

    await Provider.of<DetailTravelerViewModel>(context, listen: false).getTravelerDetail(username!, token!);
  }

  var booking = [];

  @override
  Widget build(BuildContext context) {

    final loadedTravelerModel = Provider.of<DetailTravelerViewModel>(context).detailTravelerModel;

    if(loadedTravelerModel.bookings != null) {

      booking = loadedTravelerModel.bookings!.map((item) => {
        'hotelName': item.hotelName,
        'bookingDate': item.bookingDate,
        'price': item.price,
        'numberOfPeople': item.numberOfPeople,
        'checkInDate': item.checkInDate,
        'checkOutDate': item.checkOutDate
     }).toList();
    }

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 100.0,
        elevation: 0,
        backgroundColor: AppColors.white,
        foregroundColor: AppColors.black,
        shadowColor: AppColors.white,
        title: TravelerInfoWidget(
          avatar: loadedTravelerModel.avatar ?? 'default.png',
          username: loadedTravelerModel.username ?? '',
          name: loadedTravelerModel.name ?? '',
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: booking.isEmpty
            ?  Column(
                children: [
                  Image.asset(
                    'assets/images/empty.png',
                    width: 250.0,
                  ),
                  Button(
                    title: 'OK',
                    backgroundColor: AppColors.blueButton,
                    textColor: AppColors.white,
                    fontSize: 18.0,
                    width: 80.0,
                    top: 30.0,
                    radius: 50.0,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )
                ],
            )
            :  SizedBox(
                height: 600,
                child: ListView.builder(
                  itemCount: booking.length,
                  itemBuilder: (context, index) => BookingItem(
                     hotelName: booking[index]['hotelName'],
                     bookingDate: booking[index]['bookingDate'],
                     price: booking[index]['price'],
                     numberOfPeople: booking[index]['numberOfPeople'],
                     checkInDate: booking[index]['checkInDate'],
                     checkOutDate: booking[index]['checkOutDate']
                  )
                ),
            ),
        )
      ),
    );
  }
}
