import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';

class BookingItem extends StatelessWidget {
  final String hotelName;
  final int bookingDate;
  final int price;
  final int numberOfPeople;
  final int checkInDate;
  final int checkOutDate;

  const BookingItem({
    Key? key,
    required this.hotelName,
    required this.bookingDate,
    required this.price,
    required this.numberOfPeople,
    required this.checkInDate,
    required this.checkOutDate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      margin: const EdgeInsets.only(bottom: 30.0),
      child: Column(
        children: [
          TextHeader(
            text: hotelName,
            fontWeight: FontWeight.normal,
          ),
          Table(
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            border: const TableBorder(
              horizontalInside: BorderSide(
                width: 1,
                color: AppColors.lightGrey,
                style: BorderStyle.solid
              )
            ),
            children: [
              TableRow(
                children: [
                  const TextTitle(
                    top: 10.0,
                    bottom: 10.0,
                    text: 'Book Day: ',
                    textColor: AppColors.black,
                  ),
                  TextTitle(
                    top: 10.0,
                    bottom: 10.0,
                    text: DateFormat.yMd().format(DateTime.fromMillisecondsSinceEpoch(bookingDate)),
                    textColor: AppColors.black,
                    fontWeight: FontWeight.normal,
                  ),
                ]
              ),
              TableRow(
                  children: [
                    const TextTitle(
                      top: 10.0,
                      bottom: 10.0,
                      text: 'People: ',
                      textColor: AppColors.black,
                    ),
                    TextTitle(
                      top: 10.0,
                      bottom: 10.0,
                      text: numberOfPeople.toString(),
                      textColor: AppColors.black,
                      fontWeight: FontWeight.normal,
                    ),
                  ]
              ),
              TableRow(
                  children: [
                    const TextTitle(
                      top: 10.0,
                      bottom: 10.0,
                      text: 'Price: ',
                      textColor: AppColors.black,
                    ),
                    TextTitle(
                      top: 10.0,
                      bottom: 10.0,
                      text: price.toString(),
                      textColor: AppColors.black,
                      fontWeight: FontWeight.normal,
                    ),
                  ]
              ),
              TableRow(
                  children: [
                    const TextTitle(
                      top: 10.0,
                      bottom: 10.0,
                      text: 'Check-in Day: ',
                      textColor: AppColors.black,
                    ),
                    TextTitle(
                      top: 10.0,
                      bottom: 10.0,
                      text: DateFormat.yMd().format(DateTime.fromMillisecondsSinceEpoch(checkInDate)),
                      textColor: AppColors.black,
                      fontWeight: FontWeight.normal,
                    )
                  ]
              ),
              TableRow(
                  children: [
                    const TextTitle(
                      top: 10.0,
                      bottom: 10.0,
                      text: 'Check-out Day: ',
                      textColor: AppColors.black,
                    ),
                    TextTitle(
                      top: 10.0,
                      bottom: 10.0,
                      text: DateFormat.yMd().format(DateTime.fromMillisecondsSinceEpoch(checkOutDate)),
                      textColor: AppColors.black,
                      fontWeight: FontWeight.normal,
                    ),
                  ]
              ),
            ],
          ),
        ],
      ),
    );
  }
}

