import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/modules/components/tabbar/bottom_tabbar.dart';
import 'package:where_go/services/api/account/account_services.dart';
import 'package:where_go/services/api_status.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_route.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';
import 'package:where_go/shared/responsive.dart';
import 'package:where_go/style/alert/error_alert.dart';
import 'package:where_go/style/button/button.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';
import 'package:where_go/style/textfield/textfield_account.dart';
import 'package:where_go/validation/account/account_validation.dart';
import 'package:where_go/view_models/login/login_view_model.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  String? _errorEmailText;
  String? _errorPasswordText;

  final TextEditingController _txtEmailCtrl = TextEditingController();
  final TextEditingController _txtPasswordCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    
    Responsive responsive = Responsive(context);

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                child: const TextTitle(
                  text: 'Bỏ qua',
                  alignment: Alignment.topRight,
                  underline: true,
                  top: 40.0,
                ),
                onTap: _onSkipPressed
              ),
              Image.asset(
                'assets/images/travle2.png',
                width: responsive.getOtherSize(90.0),
              ),
              const TextHeader(top: 20, text: 'Đăng nhập để bắt đầu lập kế hoạch cho chuyến đi của bạn'),
              SizedBox(height: responsive.getHeightResponsive(20)),
              TextfieldAccount(
                lable: 'Email',
                errorText: _errorEmailText,
                controller: _txtEmailCtrl,
                inputType: TextInputType.text
              ),
              SizedBox(height: responsive.getHeightResponsive(20)),
              TextfieldAccount(
                lable: 'Password',
                errorText: _errorPasswordText,
                controller: _txtPasswordCtrl,
                inputType: TextInputType.text,
                obscure: true,
              ),
              Button(
                title: 'Đăng nhập',
                backgroundColor: AppColors.bluePrimary,
                textColor: AppColors.white,
                top: 20.0,
                // onPressed: _validate,
                onPressed: login,
                width: double.infinity,
              ),
              SizedBox(height: responsive.getHeightResponsive(25)),
              GestureDetector(
                child: const TextTitle(
                  text: 'Tạo tài khoản',
                  alignment: Alignment.topLeft,
                  underline: true,
                ),
                onTap: () {
                  Navigator.pushNamed(context, AppConstantRoute.signUp);
                },
              ),
            ],
          ),
        ),
      )
    );
  }

  bool _validate() {

    bool isValid = false;

    setState(() {
      _errorEmailText = AccountValidation.isNullTextfield(_txtEmailCtrl.text, 'Email');
      _errorPasswordText = AccountValidation.isNullTextfield(_txtPasswordCtrl.text, 'Password');
    });

    if(_errorEmailText == null && _errorPasswordText == null) {
      isValid = true;
    }

    return isValid;
  }

  Future<void> login() async {
    var loginViewModel = LoginViewModel();

    if(_validate()) {
      //await loginViewModel.login('luanaaa@gmail.com', '123456');
      await loginViewModel.login(_txtEmailCtrl.text, _txtPasswordCtrl.text);
      if(loginViewModel.error == '') {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const BottomTabbar(currentTab: 0))
        );

        _txtPasswordCtrl.text = '';
      } else {
        ErrorAlert().errorAlert(context: context, content: loginViewModel.error);
      }
    }
  }

  Future<void> _onSkipPressed() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setBool(AppConstantSharedPreferences.isLogin, false);

    Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (_) => const BottomTabbar(currentTab: 0))
    );
  }
}
