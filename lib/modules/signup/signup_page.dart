import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:where_go/modules/components/tabbar/bottom_tabbar.dart';
import 'package:where_go/modules/login/login_page.dart';
import 'package:where_go/modules/signup/avatar_input_widget.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/responsive.dart';
import 'package:where_go/style/alert/error_alert.dart';
import 'package:where_go/style/alert/success_alert.dart';
import 'package:where_go/style/button/button.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/textfield/textfield_account.dart';
import 'package:where_go/validation/account/account_validation.dart';
import 'package:where_go/view_models/signup/signup_view_model.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  String? _errorEmailText;
  String? _errorPasswordText;
  String? _errorUsernameText;
  String? _errorNameText;
  String? _errorTelText;
  String?_errorDOBText;

  DateTime? dayOfBirth;

  final TextEditingController _txtEmailCtrl = TextEditingController();
  final TextEditingController _txtPasswordCtrl = TextEditingController();
  final TextEditingController _txtUsernameCtrl = TextEditingController();
  final TextEditingController _txtNameCtrl = TextEditingController();
  final TextEditingController _txtTelCtrl = TextEditingController();
  File? _pickedImage;

  void _selectImage(File pickedImage) {
    _pickedImage = pickedImage;
  }

  @override
  Widget build(BuildContext context) {
    
    Responsive responsive = Responsive(context);
    
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              TextHeader(
                text: 'Tạo tài khoản',
                top: responsive.getOtherSize(20.0),
                bottom: responsive.getOtherSize(20.0),
              ),
              AvatarInputWidget(onSelectImage: _selectImage),
              TextfieldAccount(
                lable: 'Email',
                errorText: _errorEmailText,
                controller: _txtEmailCtrl,
                inputType: TextInputType.emailAddress,
                top: 20.0,
              ),
              TextfieldAccount(
                lable: 'Passwword',
                errorText: _errorPasswordText,
                controller: _txtPasswordCtrl,
                inputType: TextInputType.text,
                top: 20.0,
                obscure: true,
              ),
              TextfieldAccount(
                lable: 'Username',
                errorText: _errorUsernameText,
                controller: _txtUsernameCtrl,
                inputType: TextInputType.text,
                top: 20.0,
              ),
              TextfieldAccount(
                lable: 'Name',
                errorText: _errorNameText,
                controller: _txtNameCtrl,
                inputType: TextInputType.text,
                top: 20.0,
                bottom: 20.0,
              ),
              GestureDetector(
                onTap: _presentDatePicker,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: responsive.getHeightResponsive(70.0),
                      padding: const EdgeInsets.all(12.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: (_errorDOBText == null) ? AppColors.grey : AppColors.errorRed,
                          width: 1.0
                        ),
                          borderRadius: BorderRadius.circular(15.0)
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          (dayOfBirth == null)
                            ? const Text(
                            'Date of birth',
                            style: TextStyle(
                              color: AppColors.grey,
                              fontSize: 15.0
                            ),
                          )
                            : Text(
                            DateFormat.yMd().format(dayOfBirth!)
                          ),
                          const Icon(Icons.calendar_today_sharp),
                        ],
                      ),
                    ),
                    (_errorDOBText == null)
                    ? Container()
                    : Padding(
                      padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                      child: Text(
                        _errorDOBText!,
                        style: const TextStyle(
                          color: AppColors.errorRed,
                          fontSize: 12.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              TextfieldAccount(
                lable: 'Telephone',
                errorText: _errorTelText,
                controller: _txtTelCtrl,
                inputType: TextInputType.phone,
                top: 20.0,
              ),
              Button(
                title: 'Đăng ký tài khoản',
                backgroundColor: AppColors.bluePrimary,
                textColor: AppColors.white,
                top: 20.0,
                onPressed: signup,
                width: double.infinity,
              )
            ],
          ),
        ),
      ),
    );
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1990),
      lastDate: DateTime.now(),
    )
        .then((pickedDate) {
      if(pickedDate == null) {
        return;
      }
      setState(() {
        dayOfBirth = pickedDate;
      });
    })
    ;
  }

  bool _validate() {

    bool isValid = true;

    setState(() {
      _errorEmailText = AccountValidation.isNullTextfield(_txtEmailCtrl.text, 'Email');
      _errorPasswordText = AccountValidation.isNullTextfield(_txtPasswordCtrl.text, 'Password');
      _errorUsernameText = AccountValidation.isNullTextfield(_txtUsernameCtrl.text, 'Username');
      _errorNameText = AccountValidation.isNullTextfield(_txtNameCtrl.text, 'Name');
      _errorTelText = AccountValidation.isNullTextfield(_txtTelCtrl.text, 'Telephone');
      if(dayOfBirth == null){
        _errorDOBText = 'Date of birth is required';
      }
      else {
        setState(() {
          _errorDOBText = null;
        });
      }
    });

    return isValid;
  }

  Future<void> signup() async {
    var signupViewModel = SignupViewModel();

    if(_validate()) {
      await signupViewModel.signup(
        _txtEmailCtrl.text,
        _txtPasswordCtrl.text,
        _txtUsernameCtrl.text,
        _txtNameCtrl.text,
        _txtTelCtrl.text,
        _pickedImage,
        dayOfBirth!
      );

      if(signupViewModel.error == '') {

        SuccessAlert().successAlert(context: context, message: 'Đăng kí tài khoản thành công.');

        Future.delayed(const Duration(seconds: 5)).then((_) {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (_) => const LoginPage())
          );
        });

      } else {
        ErrorAlert().errorAlert(context: context, content: signupViewModel.error);
      }
    }
  }
}
