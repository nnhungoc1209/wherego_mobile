import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:where_go/shared/app_colors.dart';

class AvatarInputWidget extends StatefulWidget {
  final Function onSelectImage;
  const AvatarInputWidget({Key? key, required this.onSelectImage}) : super(key: key);

  @override
  State<AvatarInputWidget> createState() => _AvatarInputWidgetState();
}

class _AvatarInputWidgetState extends State<AvatarInputWidget> {
  File? _storedImage;

  Future<void>_takePicture() async {
    final picker = ImagePicker();
    final imageFile = await picker.pickImage(
      source: ImageSource.gallery,
    );

    if(imageFile == null) {
      return;
    }

    setState(() {
      _storedImage = File(imageFile.path);
    });

    widget.onSelectImage(_storedImage);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: _takePicture,
          child: CircleAvatar(
            radius: 70.0,
            child: _storedImage == null
              ?  const Icon(
                  Icons.person_add_alt_1,
                  color: AppColors.darkGrey,
                  size: 30.0,
                )
              : null,
            backgroundColor: AppColors.lightGrey,
            backgroundImage: _storedImage != null
              ? Image.file(_storedImage!).image
              : null
          ),
        ),
      ],
    );
  }
}
