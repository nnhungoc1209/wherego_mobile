import 'package:flutter/material.dart';
import 'package:where_go/modules/components/random_items/hotel/random_hotel_list_widget.dart';
import 'package:where_go/modules/components/random_items/place/random_place_list_widget.dart';
import 'package:where_go/modules/components/random_items/restaurant/random_restaurant_list_widget.dart';
import 'package:where_go/modules/main_page/categories_item_widget.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/responsive.dart';
import 'package:where_go/style/text/text_header.dart';
import 'category_data.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(20.0),
              height: responsive.getHeightResponsive(350.0),
              width: double.infinity,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/banner.jpg'),
                  fit: BoxFit.fill,
                )
              ),
              child: ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: CATEGORIES.map((category) => CategoryItemWidget(
                  title: category.title,
                  icon: category.icon,
                  categoryRoute: category.categoryRoute,
                )).toList()
              ),
            ),

            const TextHeader(
              top: 15.0,
              text: 'Khám phá địa điểm',
              textColor: AppColors.black,
            ),
            const RandomPlaceListWidget(),

            const TextHeader(
              top: 15.0,
              text: 'Khám phá nhà hàng',
              textColor: AppColors.black,
            ),
            const RandomRestaurantListWidget(),

            const TextHeader(
              top: 15.0,
              text: 'Khám phá khách sạn',
              textColor: AppColors.black,
            ),
            const RandomHotelListWidget(),
          ],
        ),
      ),
    );
  }
}
