import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_route.dart';
import 'package:where_go/style/text/text_title.dart';

class CategoryItemWidget extends StatelessWidget {
  final String title;
  final Icon icon;
  final String categoryRoute;

  const CategoryItemWidget({
    Key? key,
    required this.title,
    required this.icon,
    required this.categoryRoute
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return UnconstrainedBox(
      child: Container(
        height: 60,
        margin: const EdgeInsets.only(right: 12.0),
        padding: const EdgeInsets.fromLTRB(18.0, 12.0, 18.0, 12.0),
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(50.0),
          border: Border.all(
              //color: AppColors.errorRed
            color: AppColors.white03
          )
        ),
        child: GestureDetector(
          onTap: () {
            //Navigator.pushNamed(context, categoryRoute);

            //Navigate to Search Page instead of its categoryRoute
            Navigator.pushNamed(context, AppConstantRoute.searchWithInput);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              icon,
              TextTitle(
                left: 10.0,
                alignment: Alignment.center,
                text: title,
                textColor: AppColors.black
              ),
              //Text(title, style: const TextStyle(color: AppColors.whiteText),)
            ],
          ),
        ),
      ),
    );
  }
}
