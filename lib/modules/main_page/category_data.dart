import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_route.dart';

class Category {
  final String title;
  final Icon icon;
  final String categoryRoute;

  const Category({
    required this.title,
    required this.icon,
    required this.categoryRoute
  });
}

const CATEGORIES = [
  Category(
      title: 'Khách Sạn',
      icon: Icon(Icons.local_hotel_outlined, color: AppColors.black06),
      categoryRoute: AppConstantRoute.hotels
  ),
  Category(
      title: 'Nhà Hàng',
      icon: Icon(Icons.restaurant_outlined, color: AppColors.black06),
      categoryRoute: AppConstantRoute.restaurants
  ),
  Category(
      title: 'Địa Điểm',
      icon: Icon(Icons.place_outlined, color: AppColors.black06),
      categoryRoute: AppConstantRoute.places
  ),
  Category(
      title: 'Bài viết',
      icon: Icon(Icons.event_note_outlined, color: AppColors.black06),
      categoryRoute: AppConstantRoute.articles
  )
];