import 'package:flutter/material.dart';
import 'package:where_go/shared/app_constant/app_constant_route.dart';

class AccountCategory {
  final String title;
  final IconData icon;
  final String categoryRoute;

  const AccountCategory({
    required this.title,
    required this.icon,
    required this.categoryRoute
  });
}

const accountCategories = [
  AccountCategory(
      title: 'Thông tin',
      icon: Icons.person_outline,
      categoryRoute: AppConstantRoute.profile
  ),
  AccountCategory(
      title: 'Booking',
      icon: Icons.calendar_today,
      categoryRoute: AppConstantRoute.booking
  ),
  AccountCategory(
      title: 'Yêu thích',
      icon: Icons.favorite_border_rounded,
      categoryRoute: AppConstantRoute.favorite
  ),
  AccountCategory(
    title: 'Đổi mật khẩu',
    icon: Icons.lock_outlined,
    categoryRoute: AppConstantRoute.changePassword
  ),
];