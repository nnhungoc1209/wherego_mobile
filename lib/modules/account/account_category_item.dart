import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/style/text/text_title.dart';

class AccountCategoryItem extends StatelessWidget {
  final String title;
  final IconData icon;
  final String route;

  const AccountCategoryItem({
    Key? key,
    required this.title,
    required this.icon,
    required this.route
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, route),
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 20.0,
          horizontal: 30.0
        ),
        child: Row(
          children: [
            Icon(
              icon,
              size: 25.0
            ),
            TextTitle(
              text: title,
              left: 35.0,
              fontSize: 19.0,
              textColor: AppColors.black,
              fontWeight: FontWeight.w500,
            )
          ],
        ),
      ),
    );
  }
}
