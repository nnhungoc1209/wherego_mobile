import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/shared/app_constant/app_constant_route.dart';
import 'package:where_go/style/button/button.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';

class AccountPageGuess extends StatelessWidget {
  const AccountPageGuess({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(
                  bottom: 20.0
              ),
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        AppColors.bluePrimary,
                        AppColors.blue1,
                        AppColors.white
                      ],
                      transform: GradientRotation(45.0)
                  ),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(45.0),
                      bottomRight: Radius.circular(45.0)
                  )
              ),
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                        top: 40.0,
                        bottom: 20.0
                    ),
                    child: const CircleAvatar(
                      radius: 70.0,
                      child: Icon(
                        Icons.person,
                        color: AppColors.darkGrey,
                        size: 30.0,
                      ),
                    ),
                  ),
                  const TextHeader(
                    text: 'Guess',
                    textColor: AppColors.black,
                    alignment: Alignment.center,
                  ),
                  const TextTitle(
                    text: 'Đăng nhập để có những trải nghiệm thú vị hơn!',
                    alignment: Alignment.center,
                    textColor: AppColors.black,
                    fontWeight: FontWeight.normal,
                    bottom: 20.0,
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: Button(
                top: 30.0,
                bottom: 30.0,
                width: double.infinity,
                height: 40.0,
                radius: 15.0,
                title: 'Đăng nhập',
                backgroundColor: AppColors.blue1,
                textColor: AppColors.black,
                onPressed: () {
                  Navigator.pushNamed(context, AppConstantRoute.login);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
