import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/modules/account/account_category_item.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';
import 'package:where_go/style/alert/confirm_alert.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';
import 'package:where_go/view_models/traveler/profile/detail_traveler_view_model.dart';
import 'account_category_data.dart';
import 'account_page_guess.dart';

class AccountPage extends StatefulWidget {
  const AccountPage({Key? key}) : super(key: key);


  @override
  State<AccountPage> createState() => _AccountPageState();
}

class _AccountPageState extends State<AccountPage> {

  bool? _isLogin;

  @override
  void initState() {
    // TODO: implement initState

    init();

    super.initState();
  }

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      _isLogin = prefs.getBool(AppConstantSharedPreferences.isLogin) ?? false;
    });

    if(_isLogin!) {
      String? username = prefs.getString(AppConstantSharedPreferences.sessionUsername);
      String? token = prefs.getString(AppConstantSharedPreferences.sessionJWTToken);

      await Provider.of<DetailTravelerViewModel>(context, listen: false).getTravelerDetail(username!, token!);
    }
  }

  @override
  Widget build(BuildContext context) {

    final loadedTravelerModel = Provider.of<DetailTravelerViewModel>(context).detailTravelerModel;

    return (_isLogin ?? false)
      ?  Scaffold(
            floatingActionButton: FloatingActionButton(
              child: const Icon(Icons.logout),
              backgroundColor: AppColors.bluePrimary,
              onPressed: logout
            ),
            body: SafeArea(
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                      bottom: 20.0
                    ),
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                          AppColors.bluePrimary,
                          AppColors.blue1,
                          AppColors.white
                        ],
                        transform: GradientRotation(45.0)
                      ),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(45.0),
                        bottomRight: Radius.circular(45.0)
                      )
                    ),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(
                            top: 40.0,
                            bottom: 20.0
                          ),
                          child: CircleAvatar(
                            radius: 70.0,
                            backgroundImage: NetworkImage(
                              AppConstantApi.baseUrl + AppConstantApi.renderImgUrl + (loadedTravelerModel.avatar ?? 'default.png'),
                            ),
                          ),
                        ),
                        TextHeader(
                          text: loadedTravelerModel.name ?? '',
                          textColor: AppColors.black,
                          alignment: Alignment.center,
                        ),
                        TextTitle(
                          text: '@${loadedTravelerModel.username ?? ''}',
                          alignment: Alignment.center,
                          textColor: AppColors.black,
                          fontWeight: FontWeight.normal,
                        ),
                        TextTitle(
                          text: loadedTravelerModel.email ?? '',
                          alignment: Alignment.center,
                          textColor: AppColors.black,
                          fontWeight: FontWeight.normal,
                          bottom: 20.0,
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 300,
                    width: 400,
                    child: ListView.builder(
                      itemCount: accountCategories.length,
                      itemBuilder: (context, index) => AccountCategoryItem(
                        title: accountCategories[index].title,
                        icon: accountCategories[index].icon,
                        route: accountCategories[index].categoryRoute,
                      )
                    ),
                  )
                ],
              ),
            ),
          )
      : const AccountPageGuess();
  }

  Future<void> logout() async {

    ConfirmAlert().confirmAlert(context: context);

  }
}
