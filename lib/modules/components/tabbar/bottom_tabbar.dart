import 'package:flutter/material.dart';
import 'package:where_go/modules/account/account_page.dart';
import 'package:where_go/modules/main_page/main_page.dart';
import 'package:where_go/modules/search/search_page.dart';
import 'package:where_go/shared/app_colors.dart';

class BottomTabbar extends StatefulWidget {
  final int currentTab;
  const BottomTabbar({Key? key, required this.currentTab}) : super(key: key);

  @override
  State<BottomTabbar> createState() => _BottomTabbarState();
}

class _BottomTabbarState extends State<BottomTabbar> {
  int _selectedPageIndex = 0;

  @override
  void initState() {
    setState(() {
      _selectedPageIndex = widget.currentTab;
    });
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  final _pageItems = [
    const MainPage(),
    const SearchPage(),
    const AccountPage()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pageItems[_selectedPageIndex],
      bottomNavigationBar: NavigationBarTheme(
        data: NavigationBarThemeData(
          indicatorColor: AppColors.white0,
          iconTheme: MaterialStateProperty.resolveWith((states) {
            if (states.contains(MaterialState.selected)) {
              return const IconThemeData(
                size: 45,
                color: AppColors.bluePrimary
              );
            }
            return const IconThemeData(
              size: 30,
              color: AppColors.grey
            );
          })
        ),
        child: NavigationBar(
          height: 60.0,
          labelBehavior: NavigationDestinationLabelBehavior.alwaysHide,
          backgroundColor: AppColors.black01,
          selectedIndex: _selectedPageIndex,
          onDestinationSelected: _selectPage,
          destinations: const [
            NavigationDestination(
              icon: Icon(Icons.home),
              label: 'Khám phá'
            ),
            NavigationDestination(
              icon: Icon(Icons.search),
              label: 'Tìm kiếm'
            ),
            NavigationDestination(
              icon: Icon(Icons.person),
              label: 'Tài khoản'
            )
          ],
        ),
      ),
    );
  }
}
