import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:where_go/modules/components/tabbar/bottom_tabbar.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_route.dart';

class TabbarWidget extends StatelessWidget {
  const TabbarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      color: AppColors.black01,
      width: screenWidth,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GestureDetector(
            child: const Icon(
              Icons.home,
              color: AppColors.grey,
              size: 30,
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) => const BottomTabbar(currentTab: 0)));
            },
          ),
          GestureDetector(
            child: const Icon(
              Icons.search,
              color: AppColors.grey,
              size: 30,
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) => const BottomTabbar(currentTab: 1)));
            },
          ),
          GestureDetector(
            child: const Icon(
              Icons.person,
              color: AppColors.grey,
              size: 30,
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) => const BottomTabbar(currentTab: 2)));
            },
          ),
        ],
      ),
    );
  }
}
