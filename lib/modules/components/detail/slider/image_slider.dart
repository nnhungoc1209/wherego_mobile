import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:where_go/models/detail/hotel/hotel_galleries_model.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/style/text/text_title.dart';

class ImageSlider extends StatefulWidget {
  final List<dynamic>? imageList;

  const ImageSlider({Key? key, required this.imageList}) : super(key: key);

  @override
  State<ImageSlider> createState() => _ImageSliderState();
}

class _ImageSliderState extends State<ImageSlider> {

  final imageUrl = [];

  @override
  Widget build(BuildContext context) {

    for(var item in widget.imageList ?? []) {
      try{
        imageUrl.add(item.label);
      } catch (e) {
        imageUrl.add(item.name);
      }
    }


    return InkWell(
      child: Image.network(
        AppConstantApi.baseUrl + AppConstantApi.renderImgUrl + (imageUrl.isNotEmpty ? imageUrl[0] : 'default.png'),
        height: 300,
        loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
          if (loadingProgress == null) {
            return child;
          }
          return Center(
            child: CircularProgressIndicator(
              value: loadingProgress.expectedTotalBytes != null
                ? loadingProgress.cumulativeBytesLoaded /
                loadingProgress.expectedTotalBytes!
                : null,
            ),
          );
        },
      ),
      onTap: openGallery,
    );
  }

  void openGallery() {
    Navigator.of(context).push(MaterialPageRoute(
      builder: (_) => GalleryWidget(
        images: imageUrl,
        index: 0,
      ),
      )
    );
  }
}

class GalleryWidget extends StatefulWidget {
  final PageController pageController;
  final List<dynamic> images;
  final int index;
  GalleryWidget({
    Key? key,
    required this.images,
    this.index = 0
  }) : pageController = PageController(initialPage: index);

  @override
  State<GalleryWidget> createState() => _GalleryWidgetState();
}

class _GalleryWidgetState extends State<GalleryWidget> {
  late int index = widget.index;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        //alignment: Alignment.bottomLeft,
        children: [
          Center(
            child: PhotoViewGallery.builder(
              pageController: widget.pageController,
              itemCount: widget.images.length,
              loadingBuilder: (BuildContext context, ImageChunkEvent? loadingProgress) {
                return Center(
                  child: CircularProgressIndicator(
                    value: loadingProgress?.expectedTotalBytes != null
                      ? loadingProgress!.cumulativeBytesLoaded /
                      loadingProgress.expectedTotalBytes!
                      : null,
                  ),
                );
              },
              builder: (context, index) {
                final urlImage = widget.images[index];
                return PhotoViewGalleryPageOptions(
                  imageProvider: NetworkImage(
                    AppConstantApi.baseUrl + AppConstantApi.renderImgUrl + urlImage,
                  )
                );
              },
              onPageChanged: (index) => setState(() => this.index = index)
            ),
          ),
          Positioned(
            bottom: 12.0,
            child: Container(
              padding: const EdgeInsets.all(16),
              child: TextTitle(
                text: '${index + 1}/${widget.images.length}',
                textColor: AppColors.whiteText,
              ),
            ),
          )
        ]
      ),
    );
  }
}


