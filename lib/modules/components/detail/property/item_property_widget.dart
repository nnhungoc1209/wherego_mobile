import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/style/text/text_title.dart';

class ItemPropertyWidget extends StatelessWidget {
  final List property;
  const ItemPropertyWidget({Key? key, required this.property}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight= MediaQuery.of(context).size.height;

    return SizedBox(
      width: screenWidth - 40,
      height: screenHeight * 0.1,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: property.map((item) => UnconstrainedBox(
          child: Container(
            padding: const EdgeInsets.all(5.0),
            margin: const EdgeInsets.symmetric(horizontal: 5.0),
            decoration: BoxDecoration(
              border: Border.all(color: AppColors.black),
              borderRadius: BorderRadius.circular(12.0),
            ),
            child: TextTitle(
              text: item,
              fontSize: 14.0,
              textColor: AppColors.black,
              fontWeight: FontWeight.normal,
            )
          ),
        )).toList(),
      ),
    );
  }
}
