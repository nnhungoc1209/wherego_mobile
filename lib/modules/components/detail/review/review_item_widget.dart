import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';

class ReviewItemWidget extends StatefulWidget {
  final int id;
  final String comment;
  final int rating;
  final String name;
  final String avatar;

  const ReviewItemWidget({
    Key? key,
    required this.id,
    required this.comment,
    required this.rating,
    required this.name,
    required this.avatar
  }) : super(key: key);

  @override
  State<ReviewItemWidget> createState() => _ReviewItemWidgetState();
}

class _ReviewItemWidgetState extends State<ReviewItemWidget> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 8.0
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 5.0,
        vertical: 5.0
      ),
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 2.0, color: AppColors.lightGrey),
        ),
      ),
      child: SizedBox(
        height: 70,
        width: screenWidth - 50.0,
        child: ListTile(
          leading: CircleAvatar(
            radius: 30,
            backgroundImage: NetworkImage(
              AppConstantApi.baseUrl + AppConstantApi.renderImgUrl + widget.avatar,
            )
          ),
          title: Text(
            widget.name,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0
            )
          ),
          trailing: SizedBox(
            width: 70.0,
            height: 260.0,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: widget.rating,
              itemBuilder: (_, index) => const Icon(
                Icons.star,
                color: AppColors.yellow,
                size: 14.0,
              ),
            ),
          ),
          subtitle: Text(widget.comment),
        ),
      ),
    );
  }
}
