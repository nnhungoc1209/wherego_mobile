import 'package:flutter/material.dart';
import 'package:where_go/modules/hotel/detail/hotel_detail_page.dart';
import 'package:where_go/modules/place/detail/place_detail_page.dart';
import 'package:where_go/modules/restaurant/detail/restaurant_detail_page.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/style/alert/error_alert.dart';
import 'package:where_go/style/alert/success_alert.dart';
import 'package:where_go/style/button/button.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/view_models/traveler/review/hotel/create_hotel_review_view_model.dart';
import 'package:where_go/view_models/traveler/review/place/create_place_review_view_model.dart';
import 'package:where_go/view_models/traveler/review/restaurant/create_restaurant_review_view_model.dart';

class WriteReviewWidget extends StatefulWidget {
  final String token;
  final String email;
  final String type;
  final int typeId;
  final bool? isLogin;

  const WriteReviewWidget({
    Key? key,
    required this.token,
    required this.email,
    required this.isLogin,
    required this.type,
    required this.typeId
  }) : super(key: key);

  @override
  State<WriteReviewWidget> createState() => _WriteReviewWidgetState();
}

class _WriteReviewWidgetState extends State<WriteReviewWidget> {

  String? _errorReviewText;
  int? rating = 5;

  final TextEditingController _txtReviewCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return (widget.isLogin!)
      ?  Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: screenWidth * 0.6,
                child: TextField(
                  controller: _txtReviewCtrl,
                  decoration: InputDecoration(
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    label: const Text('Viết đánh giá...'),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                      borderSide: const BorderSide(
                        color: AppColors.bluePrimary,
                      )
                    ),
                    errorText: _errorReviewText,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    )
                  ),
                ),
              ),
              SizedBox(
                width: screenWidth * 0.3,
                child: DropdownButton(
                  items: const [
                    DropdownMenuItem<int>(child: Text('⭐'), value: 1),
                    DropdownMenuItem<int>(child: Text('⭐⭐'), value: 2),
                    DropdownMenuItem<int>(child: Text('⭐⭐⭐'), value: 3),
                    DropdownMenuItem<int>(child: Text('⭐⭐⭐⭐'), value: 4),
                    DropdownMenuItem<int>(child: Text('⭐⭐⭐⭐⭐'), value: 5),
                  ],
                  value: rating,
                  onChanged: onChange,
                  iconSize: 24.0,
                ),
              )
            ],
          ),
          Button(
            top: 30.0,
            bottom: 30.0,
            width: double.infinity,
            height: 40.0,
            radius: 25.0,
            title: 'Đánh giá',
            backgroundColor: AppColors.blue1,
            textColor: AppColors.black,
            onPressed: onReviewPressed
          )
        ],
    )
      :  const Center(
          child: TextHeader(text: 'Đăng nhập để đánh giá',),
        );
  }

  void onChange(int? selectedRating) {
    setState(() {
      rating = selectedRating;
    });
  }

  Future<void> onReviewPressed() async {
    if(_txtReviewCtrl.text.isNotEmpty) {

      if(widget.type == 'hotel') {
        var createHotelReviewViewModel = CreateHotelReviewViewModel();
        await createHotelReviewViewModel.createHotelReview(widget.token, widget.email, widget.typeId, _txtReviewCtrl.text, rating ?? 0);

        if(createHotelReviewViewModel.error == '') {
          SuccessAlert().successAlert(context: context, message: 'Đánh giá thành công!');

          Future.delayed(const Duration(seconds: 5)).then((_) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (_) => HotelDetailPage(hotelId: widget.typeId))
            );
          });

        } else {
          ErrorAlert().errorAlert(context: context, content: createHotelReviewViewModel.error);
        }
      }

      if(widget.type == 'restaurant') {
        var createRestaurantReviewViewModel = CreateRestaurantReviewViewModel();
        await createRestaurantReviewViewModel.createRestaurantReview(widget.token, widget.email, widget.typeId, _txtReviewCtrl.text, rating ?? 0);

        if(createRestaurantReviewViewModel.error == '') {
          SuccessAlert().successAlert(context: context, message: 'Đánh giá thành công!');

          Future.delayed(const Duration(seconds: 5)).then((_) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (_) => RestaurantDetailPage(restaurantId: widget.typeId))
            );
          });

        } else {
          ErrorAlert().errorAlert(context: context, content: createRestaurantReviewViewModel.error);
        }
      }

      if(widget.type == 'place') {
        var createPlaceReviewViewModel = CreatePlaceReviewViewModel();
        await createPlaceReviewViewModel.createPlaceReview(widget.token, widget.email, widget.typeId, _txtReviewCtrl.text, rating ?? 0);

        if(createPlaceReviewViewModel.error == '') {
          SuccessAlert().successAlert(context: context, message: 'Đánh giá thành công!');

          Future.delayed(const Duration(seconds: 5)).then((_) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (_) => PlaceDetailPage(placeId: widget.typeId))
            );
          });

        } else {
          ErrorAlert().errorAlert(context: context, content: createPlaceReviewViewModel.error);
        }
      }

    } else {
      setState(() {
        _errorReviewText = 'Bạn chưa nhập đánh giá!';
      });
    }
    }
}
