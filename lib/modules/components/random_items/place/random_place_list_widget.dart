import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:where_go/modules/components/random_items/place/random_place_item_widget.dart';
import 'package:where_go/shared/responsive.dart';
import 'package:where_go/view_models/random/random_place_view_model.dart';

class RandomPlaceListWidget extends StatefulWidget {
  const RandomPlaceListWidget({Key? key}) : super(key: key);

  @override
  State<RandomPlaceListWidget> createState() => _RandomPlaceListWidgetState();
}

class _RandomPlaceListWidgetState extends State<RandomPlaceListWidget> {
  List<dynamic> _placeList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    init();

    super.initState();
  }

  Future<void> init() async {
    Future.delayed(Duration.zero).then((_) {
      Provider.of<RandomPlaceViewModel>(context, listen: false).randomPlace();
    });

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);

    _placeList = Provider.of<RandomPlaceViewModel>(context).randomPlaceModelList;

    return Container(
        padding: const EdgeInsets.all(12.0),
        height: responsive.getHeightResponsive(350.0),
        width: double.infinity,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: _placeList.map((place) => RandomPlaceItemWidget(
            id: place.id ?? 0,
            name: place.name ?? '',
            image: place.image ?? '',
            averageRating: place.averageRating ?? 0.0,
            totalRating: place.totalRating ?? 0))
          .toList(),
        ));
  }
}
