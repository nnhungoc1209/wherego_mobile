import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:where_go/models/random/random_hotel_model.dart';
import 'package:where_go/modules/components/random_items/hotel/random_hotel_item_widget.dart';
import 'package:where_go/shared/responsive.dart';
import 'package:where_go/view_models/random/random_hotel_view_model.dart';

class RandomHotelListWidget extends StatefulWidget {
  const RandomHotelListWidget({Key? key}) : super(key: key);

  @override
  State<RandomHotelListWidget> createState() => _RandomHotelListWidgetState();
}

class _RandomHotelListWidgetState extends State<RandomHotelListWidget> {
  var randomHotelModel = RandomHotelModel();

  List<dynamic> _hotelList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    init();

    super.initState();
  }

  Future<void> init() async {

    Future.delayed(Duration.zero).then((_) {
      Provider.of<RandomHotelViewModel>(context, listen: false).randomHotel();
    });

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);

    _hotelList = Provider.of<RandomHotelViewModel>(context).randomHotelModelList;

    return Container(
      padding: const EdgeInsets.all(12.0),
      height: responsive.getHeightResponsive(350.0),
      width: double.infinity,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: _hotelList.map((hotel) => RandomHotelItemWidget(
          id: hotel.id,
          name: hotel.name,
          address: hotel.address,
          hotelClass: hotel.hotelClass,
          image: hotel.image,
          price: hotel.price,
          districtName: hotel.districtName,
          averageRating: hotel.averageRating,
          totalRating: hotel.totalRating))
        .toList(),
      )
    );
  }
}
