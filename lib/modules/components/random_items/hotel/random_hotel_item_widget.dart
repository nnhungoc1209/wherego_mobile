import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:where_go/models/traveler/favorite/favorite_model.dart';
import 'package:where_go/modules/hotel/detail/hotel_detail_page.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/style/text/text_title.dart';
import 'package:where_go/view_models/traveler/favorite/favorite_provider.dart';

class RandomHotelItemWidget extends StatefulWidget {
  final int id;
  final String name;
  final String address;
  final String hotelClass;
  final String image;
  final int price;
  final String districtName;
  final double averageRating;
  final int totalRating;

  const RandomHotelItemWidget({
    Key? key,
    required this.id,
    required this.name,
    required this.address,
    required this.hotelClass,
    required this.image,
    required this.price,
    required this.districtName,
    required this.averageRating,
    required this.totalRating
  }) : super(key: key);

  @override
  State<RandomHotelItemWidget> createState() => _RandomHotelItemWidgetState();
}

class _RandomHotelItemWidgetState extends State<RandomHotelItemWidget> {

  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => HotelDetailPage(hotelId: widget.id))
        );
      },
      child: Container(
        margin: const EdgeInsets.symmetric(
          horizontal: 12.0
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              children: [
                SizedBox(
                  width: 170.0,
                  height: 170.0,
                  child: Image.network(
                    AppConstantApi.baseUrl + AppConstantApi.renderImgUrl + widget.image,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  top: 12.0,
                  right: 12.0,
                  child: GestureDetector(
                    onTap: toggleFavorite,
                    child: (isFavorite)
                        ?  const Icon(
                      Icons.favorite,
                      color: AppColors.favoriteRed,
                      size: 30.0,
                    )
                        :  const Icon(
                      Icons.favorite_border_rounded,
                      color: AppColors.favoriteRed,
                      size: 30.0,
                    ),
                  )
                )
              ],
            ),
            TextTitle(
              text: widget.name,
              height: 1.6
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                const Icon(Icons.star, color: AppColors.yellow),
                TextTitle(
                  text: widget.averageRating.toString() + ' (' + widget.totalRating.toString() + ' review)',
                  height: 1.6
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<void> toggleFavorite() async {

    setState(() {
      isFavorite = !isFavorite;
    });

    if(isFavorite) {
      //add
      Provider.of<FavoriteProvider>(context, listen: false).addFavorite(FavoriteModel(
        id: widget.id,
        image: widget.image,
        name: widget.name,
        type: 'hotel'
      ));

    } else {
      //remove
      Provider.of<FavoriteProvider>(context, listen: false).removeFavorite(widget.id);
    }
  }
}