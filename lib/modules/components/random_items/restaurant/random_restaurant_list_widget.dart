import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:where_go/modules/components/random_items/restaurant/random_restaurant_item_widget.dart';
import 'package:where_go/shared/responsive.dart';
import 'package:where_go/view_models/random/random_restaurant_view_model.dart';

class RandomRestaurantListWidget extends StatefulWidget {
  const RandomRestaurantListWidget({Key? key}) : super(key: key);

  @override
  State<RandomRestaurantListWidget> createState() =>
      _RandomRestaurantListWidgetState();
}

class _RandomRestaurantListWidgetState extends State<RandomRestaurantListWidget> {

  List<dynamic> _restaurantList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    init();

    super.initState();
  }

  Future<void> init() async {
    Future.delayed(Duration.zero).then((_) {
      Provider.of<RandomRestaurantViewModel>(context, listen: false).randomRestaurant();
    });

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);

    _restaurantList = Provider.of<RandomRestaurantViewModel>(context).randomRestaurantModelList;

    return Container(
      padding: const EdgeInsets.all(12.0),
      height: responsive.getHeightResponsive(350.0),
      width: double.infinity,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: _restaurantList.map((restaurant) => RandomRestaurantItemWidget(
          id: restaurant.id,
          name: restaurant.name,
          image: restaurant.thumbnail,
          averageRating: restaurant.averageRating,
          totalRating: restaurant.totalRating))
        .toList(),
      )
    );
  }
}
