import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';

class TravelerInfoWidget extends StatelessWidget {
  final String avatar;
  final String name;
  final String username;

  const TravelerInfoWidget({
    Key? key,
    required this.avatar,
    required this.name,
    required this.username
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 20.0,
      ),
      child: Row(
        children: [
          CircleAvatar(
            radius: 30.0,
            backgroundImage: NetworkImage(
              AppConstantApi.baseUrl + AppConstantApi.renderImgUrl + avatar,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextHeader(
                  text: name,
                  textColor: AppColors.black,
                  fontSize: 24.0,
                ),
                TextTitle(
                  text: '@$username',
                  textColor: AppColors.black,
                  fontWeight: FontWeight.normal,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
