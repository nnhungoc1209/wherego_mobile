import 'package:flutter/material.dart';
import 'package:where_go/modules/hotel/detail/hotel_detail_page.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/shared/app_constant/app_constant_route.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';

class HotelItemWidget extends StatefulWidget {
  final int id;
  final String name;
  final String address;
  final String hotelClass;
  final String image;
  final int price;
  final String districtName;
  final double averageRating;
  final int totalRating;

  const HotelItemWidget({
    Key? key,
    required this.id,
    required this.name,
    required this.address,
    required this.hotelClass,
    required this.image,
    required this.price,
    required this.districtName,
    required this.averageRating,
    required this.totalRating
  }) : super(key: key);

  @override
  State<HotelItemWidget> createState() => _HotelItemWidgetState();
}

class _HotelItemWidgetState extends State<HotelItemWidget> {
  @override
  Widget build(BuildContext context) {
    double textWidth = MediaQuery.of(context).size.width * 0.6;

    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => HotelDetailPage(hotelId: widget.id))
        );
      },
      child: Container(
        margin: const EdgeInsets.only(
          bottom: 60,
          left: 20.0,
          right: 20.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.network(AppConstantApi.baseUrl + AppConstantApi.renderImgUrl + widget.image),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                children: [
                  TextHeader(text:widget.name),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      const Icon(Icons.star, color: AppColors.yellow),
                      SizedBox(
                        width: textWidth,
                        child: TextTitle(
                          text: widget.averageRating.toString() + ' (' + widget.totalRating.toString() + ' review)',
                          height: 1.6
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      const Icon(Icons.location_on_outlined),
                      SizedBox(
                        width: textWidth,
                        child: TextTitle(
                          text: widget.address + ' - ' + widget.districtName,
                          height: 1.6
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      const Icon(Icons.attach_money_outlined),
                      SizedBox(
                        width: textWidth,
                        child: TextTitle(
                          text: widget.price.toString(),
                          height: 1.6
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const Divider(height: 3.0, thickness: 2.0)
          ],
        ),
      ),
    );
  }
}
