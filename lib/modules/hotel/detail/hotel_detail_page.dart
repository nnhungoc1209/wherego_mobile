import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/modules/components/detail/property/item_property_widget.dart';
import 'package:where_go/modules/components/detail/review/review_item_widget.dart';
import 'package:where_go/modules/components/detail/review/write_review_widget.dart';
import 'package:where_go/modules/components/detail/slider/image_slider.dart';
import 'package:where_go/modules/components/random_items/hotel/random_hotel_list_widget.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';
import 'package:where_go/view_models/detail/detail_hotel_view_model.dart';

import 'hotel_booking_widget.dart';


class HotelDetailPage extends StatefulWidget {
  final int hotelId;
  const HotelDetailPage({Key? key, required this.hotelId}) : super(key: key);

  @override
  State<HotelDetailPage> createState() => _HotelDetailPageState();
}

class _HotelDetailPageState extends State<HotelDetailPage> {
  Future? _hotelFuture;

  final roomFeatures = [];
  final roomTypes = [];
  final propertyAmenities = [];
  var review = [];

  bool? _isLogin;
  String token = '';
  String email = '';


  @override
  void initState() {
    // TODO: implement initState

    _hotelFuture = init();
    super.initState();
  }

  Future<void>init() async {
    await Provider.of<DetailHotelViewModel>(context, listen: false).detailHotel(widget.hotelId);

    final SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      _isLogin = prefs.getBool(AppConstantSharedPreferences.isLogin) ?? false;
      token = prefs.getString(AppConstantSharedPreferences.sessionJWTToken) ?? '';
      email = prefs.getString(AppConstantSharedPreferences.sessionEmail) ?? '';
    });
  }

  @override
  Widget build(BuildContext context) {

    double screenHeight= MediaQuery.of(context).size.height;

    final loadedHotelModel = Provider.of<DetailHotelViewModel>(context).detailHotelModel;

    final hotelClass = int.tryParse(loadedHotelModel.hotelClass?.substring(0,1) ?? '0');

    for(var item in loadedHotelModel.roomFeatures ?? []) {
      roomFeatures.add(item.feature);
    }

    for(var item in loadedHotelModel.roomTypes ?? []) {
      roomTypes.add(item.name);
    }

    for(var item in loadedHotelModel.propertyAmenities ?? []) {
      propertyAmenities.add(item.name);
    }

    if(loadedHotelModel.reviews != null) {
      review = loadedHotelModel.reviews!.map((item) => {
        'id': item.id,
        'comment': item.comment,
        'rating': item.rating,
        'name': item.name,
        'avatar': item.avatar
      }).toList();
    }

    return Scaffold(
      body: FutureBuilder(
        future:_hotelFuture,
        builder: (context, snapshot) => snapshot.connectionState == ConnectionState.waiting
          ? const Center(
              child: CircularProgressIndicator(),
            )
          :  SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ImageSlider(imageList: loadedHotelModel.hotelGalleries),
                  TextHeader(text: loadedHotelModel.name ?? ''),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: 100.0,
                        height: 30.0,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: hotelClass,
                          itemBuilder: (_, index) => const Icon(
                            Icons.star,
                            color: AppColors.yellow,
                            size: 18.0,
                          ),
                        ),
                      ),
                      TextHeader(
                        text: loadedHotelModel.price.toString() + '\$',
                        textColor: AppColors.black,
                      ),
                    ],
                  ),

                  _isLogin!
                  ?  HotelBookingWidget(hotelId: widget.hotelId, hotelPrice: loadedHotelModel.price ?? 0)
                  : const TextHeader(text: 'Đăng nhập để đặt phòng'),

                  const TextHeader(text: 'Giới thiệu'),
                  TextTitle(
                    text: loadedHotelModel.description.toString(),
                    fontWeight: FontWeight.normal,
                    textColor: AppColors.black,
                    bottom: 20.0,
                  ),
                  const TextTitle(text: 'Loại phòng'),
                  ItemPropertyWidget(property: roomTypes),

                  const TextTitle(text: 'Tính năng'),
                  ItemPropertyWidget(property: roomFeatures),

                  const TextTitle(text: 'Tiện nghi'),
                  ItemPropertyWidget(property: propertyAmenities),

                  const TextHeader(
                      top: 20.0,
                      text: 'Địa chỉ'
                  ),
                  TextTitle(
                    text: loadedHotelModel.address.toString(),
                    textColor: AppColors.black,
                    fontWeight: FontWeight.normal,
                  ),

                  const TextHeader(
                      top: 12.0,
                      bottom: 15.0,
                      text: 'Khách sạn bạn có thể thích'
                  ),
                  const RandomHotelListWidget(),

                  const TextHeader(
                      top: 12.0,
                      bottom: 15.0,
                      text: 'Review'
                  ),

                  WriteReviewWidget(
                    token: token,
                    email: email,
                    type: 'hotel',
                    typeId: loadedHotelModel.id ?? 0,
                    isLogin: _isLogin!
                  ),

                  review.isEmpty
                    ? const TextTitle(
                        text: 'Chưa có review!',
                        top: 5.0,
                        bottom: 20.0,
                      )
                    : SizedBox(
                      height: screenHeight * 0.5,
                      child: ListView(
                        children: review.map((item) => UnconstrainedBox(
                          child: ReviewItemWidget(
                            id: item['id'],
                            comment: item['comment'],
                            rating: item['rating'],
                            name: item['name'],
                            avatar: item['avatar'],
                          )
                        )).toList(),
                      ),
                  ),
                ],
              ),
            ),
          ),
        ),
      )
    );
  }
}
