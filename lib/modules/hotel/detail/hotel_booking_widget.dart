import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/modules/traveler/booking/booking_page.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';
import 'package:where_go/style/alert/error_alert.dart';
import 'package:where_go/style/alert/success_alert.dart';
import 'package:where_go/style/button/button.dart';
import 'package:where_go/style/text/text_title.dart';
import 'package:where_go/view_models/traveler/booking/booking_hotel_view_model.dart';

class HotelBookingWidget extends StatefulWidget {
  final int hotelId;
  final int hotelPrice;

  const HotelBookingWidget({
    Key? key,
    required this.hotelId,
    required this.hotelPrice
  }) : super(key: key);

  @override
  State<HotelBookingWidget> createState() => _HotelBookingWidgetState();
}

class _HotelBookingWidgetState extends State<HotelBookingWidget> {

  DateTime? checkInDay;
  DateTime? checkOutDay;
  int numOfPeople = 1;

  String token = '';
  String email = '';

  @override
  void initState() {

    init();

    super.initState();
  }

  Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    token = prefs.getString(AppConstantSharedPreferences.sessionJWTToken) ?? '';
    email = prefs.getString(AppConstantSharedPreferences.sessionEmail) ?? '';
  }


  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      margin: const EdgeInsets.only(
        top: 20.0
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: screenWidth * 0.3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TextTitle(
                      text: 'Check in:',
                    ),
                    GestureDetector(
                      onTap: () => _getInputDate('checkin'),
                      child: Container(
                        padding: const EdgeInsets.only(
                          top: 10.0,
                          bottom: 4.0,
                          left: 8.0,
                          right: 8.0
                        ),
                        decoration: const BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: AppColors.grey,
                              width: 1.0
                            )
                          )
                        ),
                        child: Text(
                          checkInDay == null
                            ? DateFormat.yMd().format(DateTime.now())
                            : DateFormat.yMd().format(checkInDay!)
                        )
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(
                width: screenWidth * 0.3,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const TextTitle(
                      text: 'Check out:',
                    ),
                    GestureDetector(
                      onTap: () => _getInputDate('checkout'),
                      child: Container(
                        padding: const EdgeInsets.only(
                          top: 10.0,
                          bottom: 4.0,
                          left: 8.0,
                          right: 8.0
                        ),
                        decoration: const BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: AppColors.grey,
                              width: 1.0
                            )
                        )
                      ),
                        child: Text(
                          checkOutDay == null
                            ? DateFormat.yMd().format(DateTime.now())
                            : DateFormat.yMd().format(checkOutDay!)
                        )
                    ),
                    )
                  ],
                ),
              ),

              SizedBox(
                width: screenWidth * 0.3,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    const TextTitle(
                      text: 'People:',
                    ),
                    Container(
                      padding: const EdgeInsets.only(
                        bottom: 4.0,
                        left: 8.0,
                        right: 8.0
                      ),
                      decoration: const BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: AppColors.grey,
                            width: 1.0
                          )
                        )
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            //onTap: _decrementPerson,
                            onTap: () {
                              setState(() {
                                numOfPeople--;
                              });
                            },
                            child: const Padding(
                              padding: EdgeInsets.only(
                                top:  10.0,
                                left: 5.0,
                                right: 5.0
                              ),
                              child: Icon(Icons.minimize_rounded, size: 14.0)
                            ),
                          ),
                          Text(numOfPeople.toString()),
                          GestureDetector(
                            //onTap: _incrementPerson,
                            onTap: () {
                              setState(() {
                                numOfPeople++;
                              });
                            },
                            child: const Padding(
                              padding: EdgeInsets.only(
                                top:  10.0,
                                left: 5.0,
                                right: 5.0
                              ),
                              child: Icon(Icons.add, size: 14.0)
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          Button(
            top: 20.0,
            bottom: 20.0,
            width: double.infinity,
            title: 'Đặt phòng',
            backgroundColor: AppColors.blue1,
            textColor: AppColors.black,
            radius: 25.00,
            onPressed: (checkInDay == null || checkOutDay == null) ? null : _onBookingPressed,
          ),
        ],
      )
    );
  }
  void _getInputDate(String type) {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2023)
    )
    .then((pickedDate) {
      if(pickedDate == null) {
        return;
      } else {
        if(type == 'checkin') {
          setState(() {
            checkInDay = pickedDate;
          });
        } else {
          setState(() {
            checkOutDay = pickedDate;
          });
        }
      }
    })
    ;
  }

  Future<void> _onBookingPressed() async{
    var bookingViewModel = BookingHotelViewModel();

    await bookingViewModel.bookingHotel(token, email, widget.hotelId, DateTime.now(), numOfPeople, widget.hotelPrice, checkInDay!, checkOutDay!);

    if(bookingViewModel.error == '') {
      SuccessAlert().successAlert(context: context, message: 'Đặt phòng thành công!');

      Future.delayed(const Duration(seconds: 5)).then((_) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (_) => const BookingPage())
        );
      });

    } else {
      ErrorAlert().errorAlert(context: context, content: bookingViewModel.error);
    }

  }
}
