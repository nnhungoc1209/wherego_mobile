import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/shared/app_constant/app_constant_route.dart';
import 'package:where_go/style/button/button_cate_search.dart';
import 'package:where_go/style/textfield/textfield_search.dart';

class SearchWithInputPage extends StatefulWidget {
  const SearchWithInputPage({Key? key}) : super(key: key);

  @override
  State<SearchWithInputPage> createState() => _SearchWithInputPageState();
}

class _SearchWithInputPageState extends State<SearchWithInputPage> {

  final TextEditingController _txtInputCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {

    void _clearInput() {
      setState(() {
        _txtInputCtrl.text = '';
      });
    }

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.white,
        foregroundColor: AppColors.black,
        shadowColor: AppColors.white,
        title: SizedBox(
          height: 50.0,
          width: double.infinity,
          child: TextfieldSearch(controller: _txtInputCtrl, hintText: 'Bạn sắp đến đâu?', clearInput: _clearInput),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Column(
            children: [
              ButtonCateSearch(
                title: 'Tìm kiếm trong Nhà hàng',
                icon: Icons.restaurant_outlined,
                onPressed: () => _onSearchPressed('restaurant'),
              ),
              const Divider(),
              ButtonCateSearch(
                title: 'Tìm kiếm trong Khách sạn',
                icon: Icons.hotel_outlined,
                onPressed: () => _onSearchPressed('hotel'),
              ),
              const Divider(),
              ButtonCateSearch(
                title: 'Tìm kiếm trong Địa điểm',
                icon: Icons.place_outlined,
                onPressed: () => _onSearchPressed('place'),
              ),

              const Divider(),
              ButtonCateSearch(
                title: 'Xem tất cả bài viết',
                icon: Icons.event_note_outlined,
                onPressed: () => {
                  Navigator.pushNamed(context, AppConstantRoute.articles)
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onSearchPressed(String type) async {
    String input = _txtInputCtrl.text;

    String url = AppConstantApi.baseUrl + 'search?category=$type&keyword=$input';

    try{
      Dio dio = Dio();
      var response = await dio.get(url);

      var data = {
        'input': input,
        'response': response
      };

      if(type == 'hotel') {
        Navigator.pushNamed(
          context,
          AppConstantRoute.hotels,
          arguments: data
        );
      }

      if(type == 'restaurant') {
        Navigator.pushNamed(
            context,
            AppConstantRoute.restaurants,
            arguments: data
        );
      }

      if(type == 'place') {
        Navigator.pushNamed(
            context,
            AppConstantRoute.places,
            arguments: data
        );
      }

    } catch(e) {
      print(e.toString());
    }


  }
}
