import 'package:flutter/material.dart';
import 'package:where_go/modules/components/random_items/hotel/random_hotel_list_widget.dart';
import 'package:where_go/modules/components/random_items/place/random_place_list_widget.dart';
import 'package:where_go/modules/components/random_items/restaurant/random_restaurant_list_widget.dart';
import 'package:where_go/modules/search/search_with_input_page.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              const TextHeader(text: 'Tìm kiếm', textColor: AppColors.black, top: 20.0, bottom: 20.0),
              GestureDetector(
                onTap: () => {
                  Navigator.push(
                    context, 
                    MaterialPageRoute(builder: (_) => const SearchWithInputPage())
                  )
                },
                child: Container(
                  padding: const EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1.0,
                      color: AppColors.darkGrey,
                    ),
                    borderRadius: BorderRadius.circular(25.0),
                  ),
                  child: Row(
                    children: const [
                      Icon(Icons.search),
                      TextTitle(
                        left: 15.0,
                        text: 'Bạn sắp đến đâu?',
                        textColor: AppColors.grey
                      )
                    ],
                  ),
                ),
              ),
              const TextHeader(
                top: 15.0,
                text: 'Khám phá địa điểm',
                textColor: AppColors.black,
              ),
              const RandomPlaceListWidget(),

              const TextHeader(
                top: 15.0,
                text: 'Khám phá nhà hàng',
                textColor: AppColors.black,
              ),
              const RandomRestaurantListWidget(),

              const TextHeader(
                top: 15.0,
                text: 'Khám phá khách sạn',
                textColor: AppColors.black,
              ),
              const RandomHotelListWidget(),
            ],
          ),
        ),
      )
    );
  }
}
