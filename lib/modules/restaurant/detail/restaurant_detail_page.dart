import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/modules/components/detail/property/item_property_widget.dart';
import 'package:where_go/modules/components/detail/review/review_item_widget.dart';
import 'package:where_go/modules/components/detail/review/write_review_widget.dart';
import 'package:where_go/modules/components/detail/slider/image_slider.dart';
import 'package:where_go/modules/components/random_items/restaurant/random_restaurant_list_widget.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';
import 'package:where_go/view_models/detail/detail_restaurant_view_model.dart';

class RestaurantDetailPage extends StatefulWidget {
  final int restaurantId;
  const RestaurantDetailPage({Key? key, required this.restaurantId}) : super(key: key);

  @override
  State<RestaurantDetailPage> createState() => _RestaurantDetailPageState();
}

class _RestaurantDetailPageState extends State<RestaurantDetailPage> {
  Future? _restaurantFuture;

  final cuisines = [];
  final meals = [];
  final features = [];
  final restaurantGalleries = [];
  var restaurantReviews = [];

  bool? _isLogin;
  String token = '';
  String email = '';

  @override
  void initState() {
    // TODO: implement initState

    _restaurantFuture = init();
    super.initState();
  }

  Future<void>init() async {
    await Provider.of<DetailRestaurantViewModel>(context, listen: false).detailRestaurant(widget.restaurantId);

    final SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      _isLogin = prefs.getBool(AppConstantSharedPreferences.isLogin) ?? false;
      token = prefs.getString(AppConstantSharedPreferences.sessionJWTToken) ?? '';
      email = prefs.getString(AppConstantSharedPreferences.sessionEmail) ?? '';
    });
  }

  @override
  Widget build(BuildContext context) {

    double screenHeight= MediaQuery.of(context).size.height;

    final loadedRestaurantModel = Provider.of<DetailRestaurantViewModel>(context).detailRestaurantModel;

    for(var item in loadedRestaurantModel.cuisines ?? []) {
      cuisines.add(item.type);
    }

    for(var item in loadedRestaurantModel.meals ?? []) {
      meals.add(item.type);
    }

    for(var item in loadedRestaurantModel.features ?? []) {
      features.add(item.type);
    }

    if(loadedRestaurantModel.restaurantReviews != null) {
      restaurantReviews = loadedRestaurantModel.restaurantReviews!.map((item) => {
        'id': item.id,
        'comment': item.comment,
        'rating': item.rating,
        'name': item.name,
        'avatar': item.avatar
      }).toList();
    }

    return Scaffold(
      body: FutureBuilder(
        future: _restaurantFuture,
        builder: (context, snapshot) => snapshot.connectionState == ConnectionState.waiting
            ?  const Center(
                child: CircularProgressIndicator(),
              )
            :  SafeArea(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ImageSlider(imageList: loadedRestaurantModel.restaurantGalleries),
                        TextHeader(text: loadedRestaurantModel.name ?? ''),
                        const TextHeader(text: 'Giới thiệu'),
                        const TextTitle(text: 'Phong cách ẩm thực'),
                        ItemPropertyWidget(property: cuisines),

                        const TextTitle(text: 'Món chính'),
                        ItemPropertyWidget(property: meals),

                        const TextTitle(text: 'Tiện ích'),
                        ItemPropertyWidget(property: features),

                        const TextHeader(
                            top: 20.0,
                            text: 'Địa chỉ'
                        ),
                        TextTitle(
                          text: loadedRestaurantModel.address.toString(),
                          textColor: AppColors.black,
                          fontWeight: FontWeight.normal,
                        ),

                        const TextHeader(
                            top: 12.0,
                            bottom: 15.0,
                            text: 'Nhà hàng bạn có thể thích'
                        ),
                        const RandomRestaurantListWidget(),

                        const TextHeader(
                            top: 12.0,
                            bottom: 15.0,
                            text: 'Review'
                        ),

                        WriteReviewWidget(
                          token: token,
                          email: email,
                          type: 'restaurant',
                          typeId: loadedRestaurantModel.id ?? 0,
                          isLogin: _isLogin!
                        ),

                        restaurantReviews.isEmpty
                            ? const TextTitle(
                          text: 'Chưa có review!',
                          top: 5.0,
                          bottom: 20.0,
                        )
                            : SizedBox(
                          height: screenHeight * 0.5,
                          child: ListView(
                            children: restaurantReviews.map((item) => UnconstrainedBox(
                              child: ReviewItemWidget(
                                id: item['id'],
                                comment: item['comment'],
                                rating: item['rating'],
                                name: item['name'],
                                avatar: item['avatar'],
                              )
                            )).toList(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
        ),
      ),
    );
  }
}
