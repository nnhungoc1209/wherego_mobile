import 'package:flutter/material.dart';
import 'package:where_go/modules/restaurant/restaurant_item_widget.dart';
import 'package:where_go/modules/components/tabbar/tabbar_widget.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/style/button/button.dart';
import 'package:where_go/style/text/text_appbar_title.dart';

class RestaurantsPage extends StatefulWidget {
  const RestaurantsPage({Key? key}) : super(key: key);

  @override
  State<RestaurantsPage> createState() => _RestaurantsPageState();
}

class _RestaurantsPageState extends State<RestaurantsPage> {
  @override
  Widget build(BuildContext context) {

    MediaQueryData queryData = MediaQuery.of(context);

    final data = ModalRoute.of(context)!.settings.arguments as Map;

    //Khi call API theo MVVM: chỉ nhận input  mà người dùng nhập vào thông qua ModalRoute
    //Việc call API và nhận kết qua được thực hiện trong initState() và chứa trong Provider

    var total = data['response'].data.length;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.white,
        foregroundColor: AppColors.black,
        shadowColor: AppColors.white,
        title: TextAppbarTitle(
          text: total > 0
              ?  'Có $total kết quả cho "${data['input']}"'
              :  'Không tìm thấy nhà hàng phù hợp',
          textColor: AppColors.black,
          fontSize: 20.0,

        ),
      ),
      body: Stack(
        children: [
          total > 0
              ?  Positioned(
                  top: 0,
                  bottom: 40,
                  right: 0,
                  left: 0,
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: queryData.size.height - 100,
                        child: ListView.builder(
                          itemCount: total,
                          itemBuilder: (_, index) => RestaurantItemWidget(
                            id: data['response'].data[index]['id'],
                            name: data['response'].data[index]['name'],
                            address: data['response'].data[index]['address'],
                            image: data['response'].data[index]['thumbnail'],
                            districtName: data['response'].data[index]['districtName'],
                            averageRating: data['response'].data[index]['averageRating'],
                            totalRating: data['response'].data[index]['totalRating']
                          ),
                        ),
                      )
                    ]
                ),
                )
              ),
            ),
          )
              :  Positioned(
              top: 0.0,
              bottom: 0,
              right: 0,
              left: 0,
              child: Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Column(
                  children: [
                    Image.asset(
                      'assets/images/empty.png',
                      width: 250.0,
                    ),
                    Button(
                      title: 'Tiếp tục tìm kiếm',
                      backgroundColor: AppColors.blueButton,
                      textColor: AppColors.white,
                      fontSize: 18.0,
                      width: 180.0,
                      top: 30.0,
                      radius: 50.0,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )
                  ],
                ),
              )
          ),
          const Positioned(
            height: 60,
            bottom: 0,
            child: TabbarWidget(),
          )
        ],
      ),
    );
  }
}

