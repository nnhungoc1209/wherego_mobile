import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:where_go/modules/article/detail/article_detail_page.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';

class ArticleItemWidget extends StatelessWidget {
  final int id;
  final String title;
  final String image;
  final String shortDesc;
  final int createdDate;
  final String writerName;

  const ArticleItemWidget({
    Key? key,
    required this.id,
    required this.title,
    required this.image,
    required this.shortDesc,
    required this.createdDate,
    required this.writerName
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (_) => ArticleDetailPage(articleId: id, name: writerName))
        )
      },
      child: Container(
        margin: const EdgeInsets.only(
          bottom: 60,
          left: 20.0,
          right: 20.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.network(AppConstantApi.baseUrl + AppConstantApi.renderImgUrl + image),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                children: [
                  TextHeader(
                    text:title,
                    textColor: AppColors.black
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextTitle(
                          text: writerName,
                          textColor: AppColors.black,
                        ),
                        TextTitle(
                          text: DateFormat.yMd().format(DateTime.fromMillisecondsSinceEpoch(createdDate)),
                          textColor: AppColors.black,
                        )
                      ],
                    ),
                  ),
                  TextTitle(
                    text: shortDesc,
                    textColor: AppColors.black,
                    fontWeight: FontWeight.normal,
                  )
                ],
              ),
            ),
            const Divider(height: 3.0, thickness: 2.0)
          ],
        ),
      ),
    );
  }
}
