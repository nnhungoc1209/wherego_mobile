import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:where_go/models/article/get_all_article_model.dart';
import 'package:where_go/modules/components/tabbar/tabbar_widget.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/style/text/text_appbar_title.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/view_models/article/get_all_articles_view_model.dart';

import 'article_item_widget.dart';

class ArticlesPage extends StatefulWidget {
  const ArticlesPage({Key? key}) : super(key: key);

  @override
  State<ArticlesPage> createState() => _ArticlesPageState();
}

class _ArticlesPageState extends State<ArticlesPage> {

  var articleList = GetAllArticlesModel();

  List<dynamic> _articleList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    init();

    super.initState();
  }

  Future<void> init() async {

    Future.delayed(Duration.zero).then((_) {
      Provider.of<GetAllArticlesViewModel>(context, listen: false).getAllArticles();
    });

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {

    MediaQueryData queryData = MediaQuery.of(context);
    _articleList = Provider.of<GetAllArticlesViewModel>(context).articleModelList;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.white,
        foregroundColor: AppColors.black,
        shadowColor: AppColors.white,
        title: const TextAppbarTitle(
          text: 'Tất cả bài viết',
          textColor: AppColors.black,
          fontSize: 20.0,

        ),
      ),
        body: Stack(
          children: [
            Positioned(
              top: 0,
              bottom: 40,
              right: 0,
              left: 0,
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: Center(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: queryData.size.height - 100, //sony??
                            child: ListView.builder(
                              itemCount: _articleList.length,
                              itemBuilder: (_, index) => ArticleItemWidget(
                                id: _articleList[index].id,
                                title: _articleList[index].title,
                                image: _articleList[index].image,
                                shortDesc: _articleList[index].shortDesc,
                                createdDate: _articleList[index].createdDate,
                                writerName: _articleList[index].writerName,
                              ),
                            ),
                          )
                        ]
                      ),
                    )
                ),
              ),
            ),
            const Positioned(
              height: 60,
              bottom: 0,
              child: TabbarWidget(),
            )
          ],
        )
    );
  }
}

