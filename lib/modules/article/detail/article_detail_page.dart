import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';
import 'package:where_go/view_models/article/get_article_detail_view_model.dart';

class ArticleDetailPage extends StatefulWidget {
  final int articleId;
  final String name;
  const ArticleDetailPage({Key? key, required this.articleId, required this.name}) : super(key: key);

  @override
  State<ArticleDetailPage> createState() => _ArticleDetailPageState();
}

class _ArticleDetailPageState extends State<ArticleDetailPage> {
  Future? _articleFuture;

  @override
  void initState() {
    // TODO: implement initState

    _articleFuture = init();
    super.initState();
  }

  Future<void>init() async {
    await Provider.of<GetArticleDetailViewModel>(context, listen: false).getArticleDetail(widget.articleId);
  }

  @override
  Widget build(BuildContext context) {

    final loadedArticleModel = Provider.of<GetArticleDetailViewModel>(context).articlesDetailModel;

    return Scaffold(
      body: FutureBuilder(
        future: _articleFuture,
          builder: (context, snapshot) => snapshot.connectionState == ConnectionState.waiting
              ? const Center(
                child: CircularProgressIndicator(),
              )
              : SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          TextHeader(
                            text: loadedArticleModel.title ?? '',
                            textColor: AppColors.black,
                            bottom: 12.0,
                          ),
                          Image.network(AppConstantApi.baseUrl + AppConstantApi.renderImgUrl + (loadedArticleModel.image ?? 'default.png')),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                TextTitle(
                                  text: widget.name,
                                  textColor: AppColors.black,
                                ),
                                TextTitle(
                                  text: DateFormat.yMd().format(DateTime.fromMillisecondsSinceEpoch(loadedArticleModel.createdDate ?? 0)),
                                  textColor: AppColors.black,
                                )
                              ],
                            ),
                          ),
                          TextTitle(
                            text: loadedArticleModel.shortDesc ?? '',
                            textColor: AppColors.black,
                            fontWeight: FontWeight.normal,
                          ),
                          Container(
                            child: Html(
                              data: loadedArticleModel.content,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
          )
      )
    );
  }
}
