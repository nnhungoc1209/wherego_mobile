import 'package:flutter/material.dart';
import 'package:where_go/modules/place/detail/place_detail_page.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';

class PlaceItemWidget extends StatefulWidget {
  final int id;
  final String name;
  final String image;
  final String districtName;
  final double averageRating;
  final int totalRating;

  const PlaceItemWidget({
    Key? key,
    required this.id,
    required this.name,
    required this.image,
    required this.districtName,
    required this.averageRating,
    required this.totalRating
  }) : super(key: key);

  @override
  State<PlaceItemWidget> createState() => _PlaceItemWidgetState();
}

class _PlaceItemWidgetState extends State<PlaceItemWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PlaceDetailPage(placeId: widget.id))
        );
      },
      child: Container(
        margin: const EdgeInsets.only(
          bottom: 60.0,
          left: 20.0,
          right: 20.0
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 200.0,
              child: Image.network(
                AppConstantApi.baseUrl + AppConstantApi.renderImgUrl + widget.image,
                fit: BoxFit.fitHeight,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextHeader(text:widget.name),
                  Row(
                    children: [
                      const Icon(Icons.star, color: AppColors.yellow),
                      TextTitle(
                        text: widget.averageRating.toString() + ' (' + widget.totalRating.toString() + ' review)',
                        height: 1.6
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      const Icon(Icons.location_on_outlined),
                      TextTitle(
                        text: widget.districtName,
                        height: 1.6
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const Divider(thickness: 2.0,),
          ],
        ),
      ),
    );
  }
}
