import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/modules/components/detail/property/item_property_widget.dart';
import 'package:where_go/modules/components/detail/review/review_item_widget.dart';
import 'package:where_go/modules/components/detail/review/write_review_widget.dart';
import 'package:where_go/modules/components/detail/slider/image_slider.dart';
import 'package:where_go/modules/components/random_items/place/random_place_list_widget.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';
import 'package:where_go/style/text/text_header.dart';
import 'package:where_go/style/text/text_title.dart';
import 'package:where_go/view_models/detail/detail_place_view_model.dart';

class PlaceDetailPage extends StatefulWidget {
  final int placeId;
  const PlaceDetailPage({Key? key, required this.placeId}) : super(key: key);

  @override
  State<PlaceDetailPage> createState() => _PlaceDetailPageState();
}

class _PlaceDetailPageState extends State<PlaceDetailPage> {
  Future? _placeFuture;

  var placeReviews = [];
  final placeTypes = [];

  bool? _isLogin;
  String token = '';
  String email = '';

  @override
  void initState() {
    // TODO: implement initState

    _placeFuture = init();
    super.initState();
  }

  Future<void>init() async {
    await Provider.of<DetailPlaceViewModel>(context, listen: false).detailPlace(widget.placeId);
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      _isLogin = prefs.getBool(AppConstantSharedPreferences.isLogin) ?? false;
      token = prefs.getString(AppConstantSharedPreferences.sessionJWTToken) ?? '';
      email = prefs.getString(AppConstantSharedPreferences.sessionEmail) ?? '';
    });
  }

  @override
  Widget build(BuildContext context) {

    double screenHeight= MediaQuery.of(context).size.height;

    final loadedPlaceModel = Provider.of<DetailPlaceViewModel>(context).detailPlaceModel;

    if(loadedPlaceModel.placeReviews != null) {
      placeReviews = loadedPlaceModel.placeReviews!.map((item) => {
        'id': item.id,
        'comment': item.comment,
        'rating': item.rating,
        'name': item.name,
        'avatar': item.avatar
      }).toList();
    }

    for(var item in loadedPlaceModel.placeTypes ?? []) {
      placeTypes.add(item.type);
    }

    return Scaffold(
      body: FutureBuilder(
        future: _placeFuture,
        builder: (context, snapshot) => snapshot.connectionState == ConnectionState.waiting
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      ImageSlider(imageList: loadedPlaceModel.placeGalleries),
                      TextHeader(text: loadedPlaceModel.name!),
                      const TextHeader(text: 'Giới thiệu'),
                      TextTitle(
                        text: loadedPlaceModel.description.toString(),
                        fontWeight: FontWeight.normal,
                        textColor: AppColors.black,
                        bottom: 20.0,
                      ),

                      const TextTitle(text: 'Phân loại'),
                      ItemPropertyWidget(property: placeTypes),

                      const TextHeader(
                        top: 20.0,
                        text: 'Địa chỉ'
                      ),
                      TextTitle(
                        text: loadedPlaceModel.district.toString(),
                        textColor: AppColors.black,
                        fontWeight: FontWeight.normal,
                      ),

                      const TextHeader(
                          top: 12.0,
                          bottom: 15.0,
                          text: 'Địa điểm bạn có thể thích'
                      ),
                      const RandomPlaceListWidget(),

                      const TextHeader(
                        top: 12.0,
                        bottom: 15.0,
                        text: 'Review'
                      ),

                      WriteReviewWidget(
                        token: token,
                        email: email,
                        type: 'place',
                        typeId: loadedPlaceModel.id ?? 0,
                        isLogin: _isLogin!
                      ),

                      placeReviews.isEmpty
                          ? const TextTitle(
                        text: 'Chưa có review!',
                        top: 5.0,
                        bottom: 20.0,
                      )
                          : SizedBox(
                              height: screenHeight * 0.5,
                              child: ListView(
                                children: placeReviews.map((item) => UnconstrainedBox(
                                  child: ReviewItemWidget(
                                    id: item['id'],
                                    comment: item['comment'],
                                    rating: item['rating'],
                                    name: item['name'],
                                    avatar: item['avatar'],
                                  )
                                )).toList(),
                            ),
                      ),
                    ],
                  ),
                ),
              ),
            )
      ),
    );
  }
}
