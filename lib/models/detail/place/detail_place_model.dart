import 'package:where_go/models/detail/place/place_galleries_model.dart';
import 'package:where_go/models/detail/place/place_reviews_model.dart';
import 'package:where_go/models/detail/place/place_types_model.dart';

class DetailPlaceModel {
  int? id;
  String? name;
  String? description;
  String? image;
  String? district;
  List<PlaceReviewsModel>? placeReviews;
  List<PlaceTypesModel>? placeTypes;
  List<PlaceGalleriesModel>? placeGalleries;

  DetailPlaceModel(
      {this.id,
      this.name,
      this.description,
      this.image,
      this.district,
      this.placeReviews,
      this.placeTypes,
      this.placeGalleries});

  DetailPlaceModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    image = json['image'];
    district = json['district'];
    if (json['placeReviews'] != null) {
      placeReviews = <PlaceReviewsModel>[];
      json['placeReviews'].forEach((v) {
        placeReviews!.add(PlaceReviewsModel.fromJson(v));
      });
    }
    if (json['placeTypes'] != null) {
      placeTypes = <PlaceTypesModel>[];
      json['placeTypes'].forEach((v) {
        placeTypes!.add(PlaceTypesModel.fromJson(v));
      });
    }
    if (json['placeGalleries'] != null) {
      placeGalleries = <PlaceGalleriesModel>[];
      json['placeGalleries'].forEach((v) {
        placeGalleries!.add(PlaceGalleriesModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['image'] = image;
    data['district'] = district;
    if (placeReviews != null) {
      data['placeReviews'] = placeReviews!.map((v) => v.toJson()).toList();
    }
    if (placeTypes != null) {
      data['placeTypes'] = placeTypes!.map((v) => v.toJson()).toList();
    }
    if (placeGalleries != null) {
      data['placeGalleries'] = placeGalleries!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
