class HotelGalleriesModel {
  int? id;
  String? label;

  HotelGalleriesModel({this.id, this.label});

  HotelGalleriesModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    label = json['label'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['label'] = label;
    return data;
  }
}
