class RoomFeaturesModel {
  int? id;
  String? feature;

  RoomFeaturesModel({this.id, this.feature});

  RoomFeaturesModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    feature = json['feature'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['feature'] = feature;
    return data;
  }
}
