class PropertyAmenitiesModel {
  int? id;
  String? name;

  PropertyAmenitiesModel({this.id, this.name});

  PropertyAmenitiesModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}
