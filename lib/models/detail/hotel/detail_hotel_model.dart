import 'package:where_go/models/detail/hotel/hotel_galleries_model.dart';
import 'package:where_go/models/detail/hotel/property_amenities_model.dart';
import 'package:where_go/models/detail/hotel/reviews_model.dart';
import 'package:where_go/models/detail/hotel/room_features_model.dart';
import 'package:where_go/models/detail/hotel/room_types_model.dart';

class DetailHotelModel {
  int? id;
  String? name;
  String? address;
  String? hotelClass;
  String? description;
  String? image;
  int? price;
  String? district;
  List<RoomFeaturesModel>? roomFeatures;
  List<RoomTypesModel>? roomTypes;
  List<PropertyAmenitiesModel>? propertyAmenities;
  List<HotelGalleriesModel>? hotelGalleries;
  List<ReviewsModel>? reviews;

  DetailHotelModel(
      {this.id,
      this.name,
      this.address,
      this.hotelClass,
      this.description,
      this.image,
      this.price,
      this.district,
      this.roomFeatures,
      this.roomTypes,
      this.propertyAmenities,
      this.hotelGalleries,
      this.reviews});

  DetailHotelModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address = json['address'];
    hotelClass = json['hotelClass'];
    description = json['description'];
    image = json['image'];
    price = json['price'];
    district = json['district'];
    if (json['roomFeatures'] != null) {
      roomFeatures = <RoomFeaturesModel>[];
      json['roomFeatures'].forEach((v) {
        roomFeatures!.add(RoomFeaturesModel.fromJson(v));
      });
    }
    if (json['roomTypes'] != null) {
      roomTypes = <RoomTypesModel>[];
      json['roomTypes'].forEach((v) {
        roomTypes!.add(RoomTypesModel.fromJson(v));
      });
    }
    if (json['propertyAmenities'] != null) {
      propertyAmenities = <PropertyAmenitiesModel>[];
      json['propertyAmenities'].forEach((v) {
        propertyAmenities!.add(PropertyAmenitiesModel.fromJson(v));
      });
    }
    if (json['hotelGalleries'] != null) {
      hotelGalleries = <HotelGalleriesModel>[];
      json['hotelGalleries'].forEach((v) {
        hotelGalleries!.add(HotelGalleriesModel.fromJson(v));
      });
    }
    if (json['reviews'] != null) {
      reviews = <ReviewsModel>[];
      json['reviews'].forEach((v) {
        reviews!.add(ReviewsModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['address'] = address;
    data['hotelClass'] = hotelClass;
    data['description'] = description;
    data['image'] = image;
    data['price'] = price;
    data['district'] = district;
    if (roomFeatures != null) {
      data['roomFeatures'] = roomFeatures!.map((v) => v.toJson()).toList();
    }
    if (roomTypes != null) {
      data['roomTypes'] = roomTypes!.map((v) => v.toJson()).toList();
    }
    if (propertyAmenities != null) {
      data['propertyAmenities'] =
          propertyAmenities!.map((v) => v.toJson()).toList();
    }
    if (hotelGalleries != null) {
      data['hotelGalleries'] = hotelGalleries!.map((v) => v.toJson()).toList();
    }
    if (reviews != null) {
      data['reviews'] = reviews!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
