class RestaurantReviewsModel {
  int? id;
  String? comment;
  int? rating;
  String? name;
  String? avatar;

  RestaurantReviewsModel(
      {this.id, this.comment, this.rating, this.name, this.avatar});

  RestaurantReviewsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    comment = json['comment'];
    rating = json['rating'];
    name = json['name'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['comment'] = comment;
    data['rating'] = rating;
    data['name'] = name;
    data['avatar'] = avatar;
    return data;
  }
}
