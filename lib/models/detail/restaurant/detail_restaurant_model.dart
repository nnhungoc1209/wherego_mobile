import 'package:where_go/models/detail/restaurant/cuisinses_model.dart';
import 'package:where_go/models/detail/restaurant/features_model.dart';
import 'package:where_go/models/detail/restaurant/meals_model.dart';
import 'package:where_go/models/detail/restaurant/restaurant_galleries_model.dart';
import 'package:where_go/models/detail/restaurant/restaurant_reviews_model.dart';

class DetailRestaurantModel {
  int? id;
  String? name;
  String? address;
  String? thumbnail;
  String? district;
  List<CuisinesModel>? cuisines;
  List<MealsModel>? meals;
  List<FeaturesModel>? features;
  List<RestaurantGalleriesModel>? restaurantGalleries;
  List<RestaurantReviewsModel>? restaurantReviews;

  DetailRestaurantModel(
      {this.id,
      this.name,
      this.address,
      this.thumbnail,
      this.district,
      this.cuisines,
      this.meals,
      this.features,
      this.restaurantGalleries,
      this.restaurantReviews});

  DetailRestaurantModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address = json['address'];
    thumbnail = json['thumbnail'];
    district = json['district'];
    if (json['cuisines'] != null) {
      cuisines = <CuisinesModel>[];
      json['cuisines'].forEach((v) {
        cuisines!.add(CuisinesModel.fromJson(v));
      });
    }
    if (json['meals'] != null) {
      meals = <MealsModel>[];
      json['meals'].forEach((v) {
        meals!.add(MealsModel.fromJson(v));
      });
    }
    if (json['features'] != null) {
      features = <FeaturesModel>[];
      json['features'].forEach((v) {
        features!.add(FeaturesModel.fromJson(v));
      });
    }
    if (json['restaurantGalleries'] != null) {
      restaurantGalleries = <RestaurantGalleriesModel>[];
      json['restaurantGalleries'].forEach((v) {
        restaurantGalleries!.add(RestaurantGalleriesModel.fromJson(v));
      });
    }
    if (json['restaurantReviews'] != null) {
      restaurantReviews = <RestaurantReviewsModel>[];
      json['restaurantReviews'].forEach((v) {
        restaurantReviews!.add(RestaurantReviewsModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['address'] = address;
    data['thumbnail'] = thumbnail;
    data['district'] = district;
    if (cuisines != null) {
      data['cuisines'] = cuisines!.map((v) => v.toJson()).toList();
    }
    if (meals != null) {
      data['meals'] = meals!.map((v) => v.toJson()).toList();
    }
    if (features != null) {
      data['features'] = features!.map((v) => v.toJson()).toList();
    }
    if (restaurantGalleries != null) {
      data['restaurantGalleries'] =
          restaurantGalleries!.map((v) => v.toJson()).toList();
    }
    if (restaurantReviews != null) {
      data['restaurantReviews'] =
          restaurantReviews!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
