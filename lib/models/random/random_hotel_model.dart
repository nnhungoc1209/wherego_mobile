class RandomHotelModel {
  int? id;
  String? name;
  String? address;
  String? hotelClass;
  String? image;
  int? price;
  String? districtName;
  List<String>? roomFeatures;
  List<String>? roomTypes;
  List<String>? propertyAmenities;
  double? averageRating;
  int? totalRating;

  RandomHotelModel({
    this.id,
    this.name,
    this.address,
    this.hotelClass,
    this.image,
    this.price,
    this.districtName,
    this.roomFeatures,
    this.roomTypes,
    this.propertyAmenities,
    this.averageRating,
    this.totalRating
  });

  RandomHotelModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address = json['address'];
    hotelClass = json['hotelClass'];
    image = json['image'];
    price = json['price'];
    districtName = json['districtName'];
    roomFeatures = json['roomFeatures'].cast<String>();
    roomTypes = json['roomTypes'].cast<String>();
    propertyAmenities = json['propertyAmenities'].cast<String>();
    averageRating = json['averageRating'];
    totalRating = json['totalRating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['address'] = address;
    data['hotelClass'] = hotelClass;
    data['image'] = image;
    data['price'] = price;
    data['districtName'] = districtName;
    data['roomFeatures'] = roomFeatures;
    data['roomTypes'] = roomTypes;
    data['propertyAmenities'] = propertyAmenities;
    data['averageRating'] = averageRating;
    data['totalRating'] = totalRating;
    return data;
  }
}