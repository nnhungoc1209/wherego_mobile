class RandomRestaurantModel {
  int? id;
  String? name;
  String? address;
  String? thumbnail;
  String? districtName;
  List<String>? cuisines;
  List<String>? meals;
  List<String>? features;
  double? averageRating;
  int? totalRating;

  RandomRestaurantModel(
      {this.id,
      this.name,
      this.address,
      this.thumbnail,
      this.districtName,
      this.cuisines,
      this.meals,
      this.features,
      this.averageRating,
      this.totalRating});

  RandomRestaurantModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    address = json['address'];
    thumbnail = json['thumbnail'];
    districtName = json['districtName'];
    cuisines = json['cuisines'].cast<String>();
    meals = json['meals'].cast<String>();
    features = json['features'].cast<String>();
    averageRating = json['averageRating'];
    totalRating = json['totalRating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['address'] = address;
    data['thumbnail'] = thumbnail;
    data['districtName'] = districtName;
    data['cuisines'] = cuisines;
    data['meals'] = meals;
    data['features'] = features;
    data['averageRating'] = averageRating;
    data['totalRating'] = totalRating;
    return data;
  }
}
