class RandomPlaceModel {
  int? id;
  String? name;
  String? image;
  String? districtName;
  List<String>? placeTypes;
  double? averageRating;
  int? totalRating;

  RandomPlaceModel(
      {this.id,
      this.name,
      this.image,
      this.districtName,
      this.placeTypes,
      this.averageRating,
      this.totalRating});

  RandomPlaceModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    districtName = json['districtName'];
    placeTypes = json['placeTypes'].cast<String>();
    averageRating = json['averageRating'];
    totalRating = json['totalRating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['image'] = image;
    data['districtName'] = districtName;
    data['placeTypes'] = placeTypes;
    data['averageRating'] = averageRating;
    data['totalRating'] = totalRating;
    return data;
  }
}
