class LoginModel {
  int? statusCode;
  String? message;
  String? token;

  LoginModel({this.statusCode, this.message, this.token});

  LoginModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    message = json['message'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['statusCode'] = statusCode;
    data['message'] = message;
    data['token'] = token;
    return data;
  }
}