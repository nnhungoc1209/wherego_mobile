class ArticlesDetailModel {
  int? id;
  String? title;
  String? image;
  String? content;
  String? shortDesc;
  int? createdDate;
  String? name;

  ArticlesDetailModel(
      {this.id,
        this.title,
        this.image,
        this.content,
        this.shortDesc,
        this.createdDate,
        this.name});

  ArticlesDetailModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    image = json['image'];
    content = json['content'];
    shortDesc = json['shortDesc'];
    createdDate = json['createdDate'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['image'] = image;
    data['content'] = content;
    data['shortDesc'] = shortDesc;
    data['createdDate'] = createdDate;
    data['name'] = name;
    return data;
  }
}