class GetAllArticlesModel {
  int? id;
  String? title;
  String? image;
  String? shortDesc;
  int? createdDate;
  String? writerName;

  GetAllArticlesModel(
      {this.id,
        this.title,
        this.image,
        this.shortDesc,
        this.createdDate,
        this.writerName});

  GetAllArticlesModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    image = json['image'];
    shortDesc = json['shortDesc'];
    createdDate = json['createdDate'];
    writerName = json['writerName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['image'] = image;
    data['shortDesc'] = shortDesc;
    data['createdDate'] = createdDate;
    data['writerName'] = writerName;
    return data;
  }
}