class FavoriteModel {
  int? id;
  String? name;
  String? image;
  String? type;

  FavoriteModel({this.id, this.name, this.image, this.type});

  FavoriteModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['image'] = image;
    data['type'] = type;
    return data;
  }
}