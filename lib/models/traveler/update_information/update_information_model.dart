class UpdateInformationModel {
  int? statusCode;
  String? message;

  UpdateInformationModel({this.statusCode, this.message});

  UpdateInformationModel.fromJson(Map<String, dynamic> json) {
    statusCode = json['statusCode'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['statusCode'] = statusCode;
    data['message'] = message;
    return data;
  }
}