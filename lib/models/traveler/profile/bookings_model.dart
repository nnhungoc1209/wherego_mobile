class BookingsModel {
  String? hotelName;
  int? bookingDate;
  int? price;
  int? numberOfPeople;
  int? checkInDate;
  int? checkOutDate;

  BookingsModel({
    this.hotelName,
    this.bookingDate,
    this.price,
    this.numberOfPeople,
    this.checkInDate,
    this.checkOutDate
  });

  BookingsModel.fromJson(Map<String, dynamic> json) {
    hotelName = json['hotelName'];
    bookingDate = json['bookingDate'];
    price = json['price'];
    numberOfPeople = json['numberOfPeople'];
    checkInDate = json['checkInDate'];
    checkOutDate = json['checkOutDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['hotelName'] = hotelName;
    data['bookingDate'] = bookingDate;
    data['price'] = price;
    data['numberOfPeople'] = numberOfPeople;
    data['checkInDate'] = checkInDate;
    data['checkOutDate'] = checkOutDate;
    return data;
  }
}