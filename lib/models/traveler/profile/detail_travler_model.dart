import 'bookings_model.dart';

class DetailTravelerModel {
  String? email;
  String? username;
  String? name;
  String? tel;
  String? avatar;
  int? dob;
  List<BookingsModel>? bookings;

  DetailTravelerModel({
    this.email,
    this.username,
    this.name,
    this.tel,
    this.avatar,
    this.dob,
    this.bookings
  });

  DetailTravelerModel.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    username = json['username'];
    name = json['name'];
    tel = json['tel'];
    avatar = json['avatar'];
    dob = json['dob'];
    if (json['bookings'] != null) {
      bookings = <BookingsModel>[];
      json['bookings'].forEach((v) {
        bookings!.add(BookingsModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['username'] = username;
    data['name'] = name;
    data['tel'] = tel;
    data['avatar'] = avatar;
    data['dob'] = dob;
    if (bookings != null) {
      data['bookings'] = bookings!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}