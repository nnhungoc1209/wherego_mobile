import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/responsive.dart';

class TextfieldAccount extends StatelessWidget {

  final String? lable;
  final String? errorText;
  final TextInputType? inputType;
  final TextEditingController? controller;
  final bool obscure;
  final double? top;
  final double? bottom;
  final double? left;
  final double? right;
  final IconData? suffixIcon;
  final FloatingLabelBehavior? floatingLabelBehavior;

  const TextfieldAccount({
    Key? key,
    this.lable,
    this.errorText,
    this.inputType,
    this.controller,
    this.obscure = false,
    this.top,
    this.bottom,
    this.left,
    this.right,
    this.suffixIcon,
    this.floatingLabelBehavior
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    Responsive responsive = Responsive(context);

    return Container(
      margin: EdgeInsets.only(
        top: responsive.getOtherSize(top ?? 0),
        bottom: responsive.getOtherSize(bottom ?? 0),
        left: responsive.getOtherSize(left ?? 0),
        right: responsive.getOtherSize(right ?? 0),
      ),
      child: TextField(
        controller: controller,
        obscureText: obscure,
        decoration: InputDecoration(
          floatingLabelBehavior: floatingLabelBehavior,
          suffixIcon: Icon(suffixIcon),
          label: Text(lable ??  ''),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
            borderSide: const BorderSide(
              color: AppColors.bluePrimary,
            )
          ),
          errorText: errorText,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15.0),
          )
        ),
      ),
    );
  }
}

