import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';

class TextfieldSearch extends StatefulWidget {
  final TextEditingController controller;
  final String hintText;
  final Function clearInput;

  const TextfieldSearch({
    Key? key,
    required this.controller,
    required this.hintText,
    required this.clearInput,
  }) : super(key: key);

  @override
  State<TextfieldSearch> createState() => _TextfieldSearchState();
}

class _TextfieldSearchState extends State<TextfieldSearch> {

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.controller,
      decoration: InputDecoration(
        hintText: widget.hintText,
        prefixIcon: const Icon(Icons.search, color: AppColors.black),
        suffixIcon: IconButton(
          icon: const Icon(Icons.clear, color: AppColors.black),
          onPressed: () {
            setState(() {
              widget.clearInput();
            });
          },
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
          borderSide: const BorderSide(
            color: AppColors.lightGrey,
          )
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25.0),
          borderSide: const BorderSide(
            color: AppColors.lightGrey,
          )
        ),
      ),
    );
  }
}

