import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/responsive.dart';

class TextTitle extends StatelessWidget {
  final String text;
  final bool underline;
  final double? top;
  final double? bottom;
  final double? left;
  final double? right;
  final Alignment? alignment;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final Color? textColor;
  final double? height;
  final double? fontSize;

  const TextTitle({
    Key? key,
    required this.text,
    this.underline = false,
    this.top,
    this.bottom,
    this.left,
    this.right,
    this.alignment,
    this.textAlign,
    this.fontWeight,
    this.textColor,
    this.height,
    this.fontSize
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);

    return Container(
      padding: EdgeInsets.only(
        top: top ?? 0,
        bottom: bottom ?? 0,
        left: left ?? 0,
        right: right ?? 0,
      ),
      child: Align(
        alignment: alignment ?? Alignment.topLeft,
        child: Text(
          text,
          overflow: TextOverflow.visible,
          style: TextStyle(
            color: textColor ?? AppColors.bluePrimary,
            fontSize: fontSize ?? responsive.getOtherSize(15.0),
            fontWeight: fontWeight ?? FontWeight.bold,
            height: height ?? 1.25,
            decoration: (underline) ? TextDecoration.underline : TextDecoration.none
          ),
        ),
      )
    );
  }
}
