import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/responsive.dart';

class TextHeader extends StatelessWidget {
  final String text;
  final double? top;
  final double? bottom;
  final double? left;
  final double? right;
  final Alignment? alignment;
  final TextAlign? textAlign;
  final FontWeight? fontWeight;
  final Color? textColor;
  final double? fontSize;

  const TextHeader({
    Key? key,
    required this.text,
    this.top,
    this.bottom,
    this.left,
    this.right,
    this.alignment,
    this.textAlign,
    this.fontWeight,
    this.textColor,
    this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Responsive responsive = Responsive(context);

    return Container(
      padding: EdgeInsets.only(
        top: top ?? 0,
        bottom: bottom ?? 0,
        left: left ?? 0,
        right: right ?? 0,
      ),
      child: Align(
        alignment: alignment ?? Alignment.topLeft,
        child: Text(
          text,
          style: TextStyle(
            color: textColor ?? AppColors.bluePrimary,
            fontSize: fontSize ?? responsive.getOtherSize(26.0),
            fontWeight: fontWeight ?? FontWeight.bold,
            height: 1.25,
          ),
        ),
      )
    );
  }
}

