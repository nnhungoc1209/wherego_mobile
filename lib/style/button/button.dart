import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/responsive.dart';

class Button extends StatelessWidget {
  final String title;
  final Color backgroundColor;
  final Color textColor;
  final double? top;
  final double? bottom;
  final double? left;
  final double? right;
  final double? width;
  final double? height;
  final VoidCallback? onPressed;
  final AlignmentGeometry? aligment;
  final double? fontSize;
  final double? radius;

  const Button({
    Key? key,
    required this.title,
    required this.backgroundColor,
    required this.textColor,
    this.top,
    this.bottom,
    this.left,
    this.right,
    this.width,
    this.height,
    this.onPressed,
    this.aligment,
    this.fontSize,
    this.radius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    Responsive responsive = Responsive(context);

    return Container(
      alignment: aligment ?? Alignment.center,
      margin: EdgeInsets.only(
        top: top ?? 0,
        bottom: bottom ?? 0,
        left: left ?? 0,
        right: right ?? 0
      ),
      child: SizedBox(
        width: responsive.getWidthResponsive(width ?? 100),
        height: responsive.getHeightResponsive(height ?? 50),
        child: ElevatedButton(
          style: ButtonStyle(
            shadowColor: MaterialStateProperty.all<Color>(Colors.transparent),
            backgroundColor: MaterialStateProperty.all<Color>(backgroundColor),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(radius ?? 8.0),
                side: BorderSide(
                  color: backgroundColor == AppColors.white ? textColor : backgroundColor,
                  width: 1.0,
                )
              )
            )
          ),
          onPressed: onPressed,

          child: Text(
            title,
            style: TextStyle(
              color: textColor,
              fontSize: fontSize ?? responsive.getOtherSize(16),
              fontWeight: FontWeight.w400,
            ),
          )
        ),
      ),
    );
  }
}

