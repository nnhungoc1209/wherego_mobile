import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/responsive.dart';

class ButtonCateSearch extends StatelessWidget {
  final String title;
  final Color? backgroundColor;
  final Color? textColor;
  final IconData? icon;
  final double? top;
  final double? bottom;
  final double? left;
  final double? right;
  final double? width;
  final double? height;
  final VoidCallback? onPressed;
  final AlignmentGeometry? aligment;

  const ButtonCateSearch({
    Key? key,
    required this.title,
    this.backgroundColor,
    this.textColor,
    this.icon,
    this.top,
    this.bottom,
    this.left,
    this.right,
    this.width,
    this.height,
    this.onPressed,
    this.aligment
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    Responsive responsive = Responsive(context);

    return Container(
      alignment: aligment ?? Alignment.center,
      margin: EdgeInsets.only(
        top: top ?? 20,
        bottom: bottom ?? 0,
        left: left ?? 0,
        right: right ?? 0
      ),
      child: SizedBox(
        width: double.infinity,
        height: responsive.getHeightResponsive(height ?? 50),
        child: ElevatedButton(
          style: ButtonStyle(
            shadowColor: MaterialStateProperty.all<Color>(Colors.transparent),
            backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
          ),
          onPressed: onPressed,
          child: Row(
            children: [
              Icon(icon, size: 24.0, color: AppColors.darkGrey),
              const SizedBox(width: 20.0),
              Text(
                title,
                style: TextStyle(
                  color: AppColors.black,
                  fontSize: responsive.getOtherSize(14.0),
                  fontWeight: FontWeight.normal,
                ),
              )
            ]
          )
        ),
      ),
    );
  }
}

