import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';

class ErrorAlert {
  void errorAlert({required BuildContext context, required String content}) {

    Widget okButton = TextButton(
      onPressed: () {
        Navigator.pop(context);
      },
      child: const Text('OK')
    );

    AlertDialog dialog = AlertDialog(
      title: Row(
        children: const [
          Icon(
            Icons.error_outline_outlined,
            color: AppColors.bluePrimary,
          ),
          Text(
            '  Lỗi',
            style: TextStyle(
              color: AppColors.bluePrimary
            ),
          )
        ],
      ),
      content: Text(content),
      actions: [okButton],
    );

    showDialog(
      context: context,
      builder: (_) {
        return dialog;
      }
    );
  }
}