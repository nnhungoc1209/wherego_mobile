import 'package:flutter/material.dart';
import 'package:where_go/shared/app_colors.dart';

class SuccessAlert {
  void successAlert({required BuildContext context, required String message}) {

    Widget okButton = TextButton(
        onPressed: () async {
          Navigator.pop(context);
        },
        child: const Text('OK')
    );

    AlertDialog dialog = AlertDialog(
      title: Row(
        children: const [
          Icon(
            Icons.check_circle,
            color: AppColors.bluePrimary,
          ),
          Text(
            ' Thành công',
            style: TextStyle(
                color: AppColors.bluePrimary
            ),
          )
        ],
      ),
      content: Text(message),
      actions: [okButton],
    );

    showDialog(
        context: context,
        builder: (_) {
          return dialog;
        }
    );
  }
}