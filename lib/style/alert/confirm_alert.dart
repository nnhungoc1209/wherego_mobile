import 'package:flutter/material.dart';
import 'package:where_go/modules/components/tabbar/bottom_tabbar.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/view_models/traveler/profile/detail_traveler_view_model.dart';
import 'package:provider/provider.dart';
import 'package:where_go/view_models/traveler/favorite/favorite_provider.dart';


class ConfirmAlert {
  void confirmAlert({required BuildContext context}) {

    Widget okButton = TextButton(
      onPressed: () async {
        Navigator.pop(context);

        DetailTravelerViewModel detailTravelerViewModel = DetailTravelerViewModel();

        await detailTravelerViewModel.logout();
        Provider.of<FavoriteProvider>(context, listen: false).removeFavoriteList();

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (_) => const BottomTabbar(currentTab: 0))
        );
      },
        child: const Text('OK')
    );

    AlertDialog dialog = AlertDialog(
      title: Row(
        children: const [
          Icon(
            Icons.question_mark_outlined,
            color: AppColors.bluePrimary,
          ),
          Text(
            ' Xác nhận',
            style: TextStyle(
                color: AppColors.bluePrimary
            ),
          )
        ],
      ),
      content: const Text('Bạn chắc chắn đăng xuất?'),
      actions: [okButton],
    );

    showDialog(
        context: context,
        builder: (_) {
          return dialog;
        }
    );
  }
}