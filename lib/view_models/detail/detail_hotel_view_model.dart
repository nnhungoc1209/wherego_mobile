import 'package:flutter/cupertino.dart';
import 'package:where_go/models/detail/hotel/detail_hotel_model.dart';
import 'package:where_go/services/api/detail/detail_services.dart';
import 'package:where_go/services/api_status.dart';

class DetailHotelViewModel extends ChangeNotifier {
  DetailHotelModel detailHotelModel = DetailHotelModel();

  String error = '';

  detailHotel(int id) async {
    var result = await DetailServices().detailHotel(id);

    if (result is Failure) {
      error = result.errorResponse.toString();
      detailHotelModel = DetailHotelModel();
    }

    if (result is Success) {
      error = '';
      detailHotelModel = result.response as DetailHotelModel;
    }

    notifyListeners();
  }
}
