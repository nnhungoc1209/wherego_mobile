import 'package:flutter/cupertino.dart';
import 'package:where_go/models/detail/restaurant/detail_restaurant_model.dart';
import 'package:where_go/services/api/detail/detail_services.dart';
import 'package:where_go/services/api_status.dart';

class DetailRestaurantViewModel extends ChangeNotifier {
  DetailRestaurantModel detailRestaurantModel = DetailRestaurantModel();

  String error = '';

  detailRestaurant(int id) async {
    var result = await DetailServices().detailRestaurant(id);

    if (result is Failure) {
      error = result.errorResponse.toString();
      detailRestaurantModel = DetailRestaurantModel();
    }

    if (result is Success) {
      error = '';
      detailRestaurantModel = result.response as DetailRestaurantModel;
    }

    notifyListeners();
  }
}
