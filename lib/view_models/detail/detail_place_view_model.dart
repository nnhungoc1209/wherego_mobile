import 'package:flutter/cupertino.dart';
import 'package:where_go/models/detail/place/detail_place_model.dart';
import 'package:where_go/services/api/detail/detail_services.dart';
import 'package:where_go/services/api_status.dart';

class DetailPlaceViewModel extends ChangeNotifier {
  DetailPlaceModel detailPlaceModel = DetailPlaceModel();

  String error = '';

  detailPlace(int id) async {
    var result = await DetailServices().detailPlace(id);

    if (result is Failure) {
      error = result.errorResponse.toString();
      detailPlaceModel = DetailPlaceModel();
    }

    if (result is Success) {
      error = '';
      detailPlaceModel = result.response as DetailPlaceModel;
    }

    notifyListeners();
  }
}
