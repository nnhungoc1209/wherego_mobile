import 'package:flutter/cupertino.dart';
import 'package:where_go/services/api/random/random_services.dart';
import 'package:where_go/services/api_status.dart';

class RandomPlaceViewModel extends ChangeNotifier {
  List<dynamic> randomPlaceModelList = [];

  String error = '';

  randomPlace() async {
    var result = await RandomServices().randomPlace();

    if (result is Failure) {
      error = result.errorResponse.toString();
      randomPlaceModelList = [];
    }

    if (result is Success) {
      error = '';
      randomPlaceModelList = result.response as List<dynamic>;
    }

    notifyListeners();
  }
}
