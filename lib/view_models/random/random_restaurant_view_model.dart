import 'package:flutter/cupertino.dart';
import 'package:where_go/services/api/random/random_services.dart';
import 'package:where_go/services/api_status.dart';

class RandomRestaurantViewModel extends ChangeNotifier {
  List<dynamic> randomRestaurantModelList = [];

  String error = '';

  randomRestaurant() async {
    var result = await RandomServices().randomRestaurant();

    if (result is Failure) {
      error = result.errorResponse.toString();
      randomRestaurantModelList = [];
    }

    if (result is Success) {
      error = '';
      randomRestaurantModelList = result.response as List<dynamic>;
    }

    notifyListeners();
  }
}
