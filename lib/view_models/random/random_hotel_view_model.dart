import 'package:flutter/cupertino.dart';
import 'package:where_go/services/api/random/random_services.dart';
import 'package:where_go/services/api_status.dart';

class RandomHotelViewModel extends ChangeNotifier {
  List<dynamic> randomHotelModelList = [];

  String error = '';

  randomHotel() async {
    var result = await RandomServices().randomHotel();

    if (result is Failure) {
      error = result.errorResponse.toString();
      randomHotelModelList = [];
    }

    if (result is Success) {
      error = '';
      randomHotelModelList = result.response as List<dynamic>;
    }

    notifyListeners();
  }
}
