import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/models/traveler/profile/detail_travler_model.dart';
import 'package:where_go/services/api/traveler/traveler_services.dart';
import 'package:where_go/services/api_status.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';

class DetailTravelerViewModel extends ChangeNotifier {
  DetailTravelerModel detailTravelerModel = DetailTravelerModel();

  String error = '';

  getTravelerDetail(String username, String token) async {
    var result = await TravelerServices().getTravelerDetail(username, token);

    if (result is Failure) {
      error = result.errorResponse.toString();
      detailTravelerModel = DetailTravelerModel();
    }

    if (result is Success) {
      error = '';
      detailTravelerModel = result.response as DetailTravelerModel;
    }

    notifyListeners();
  }

  Future<void> logout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setBool(AppConstantSharedPreferences.isLogin, false);

    await prefs.remove(AppConstantSharedPreferences.sessionEmail);
    await prefs.remove(AppConstantSharedPreferences.sessionPassword);
    await prefs.remove(AppConstantSharedPreferences.sessionUsername);
    await prefs.remove(AppConstantSharedPreferences.sessionJWTToken);

    notifyListeners();

  }
}