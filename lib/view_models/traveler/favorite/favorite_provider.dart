import 'package:flutter/material.dart';
import 'package:where_go/models/traveler/favorite/favorite_model.dart';

class FavoriteProvider extends ChangeNotifier {
  List<FavoriteModel> favoriteList = [];

  //Add and remove Favorite
  addFavorite(FavoriteModel item) {
    favoriteList.add(item);

    notifyListeners();
  }

  removeFavorite(int id) {
    final itemIndex = favoriteList.indexWhere((item) => item.id == id);
    favoriteList.removeAt(itemIndex);

    notifyListeners();
  }

  //Remove all item
  removeFavoriteList() {
    favoriteList.clear();
  }
}