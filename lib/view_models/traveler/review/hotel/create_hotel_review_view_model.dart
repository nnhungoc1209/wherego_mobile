import 'package:flutter/material.dart';
import 'package:where_go/models/traveler/review/hotel/create_hotel_review_model.dart';
import 'package:where_go/services/api/traveler/review/hotel/hotel_review_services.dart';
import 'package:where_go/services/api_status.dart';

class CreateHotelReviewViewModel extends ChangeNotifier {

  CreateHotelReviewModel createHotelReviewModel = CreateHotelReviewModel();

  String error = '';

  createHotelReview(String token, String email, int hotelId, String comment, int rating) async {

    var result = await HotelReviewServices().createHotelReview(token, email, hotelId, comment, rating);

    if (result is Failure) {
      error = result.errorResponse.toString();
      createHotelReviewModel = CreateHotelReviewModel();
    }

    if (result is Success) {
      error = '';
      createHotelReviewModel = result.response as CreateHotelReviewModel;
    }

    notifyListeners();

  }
}