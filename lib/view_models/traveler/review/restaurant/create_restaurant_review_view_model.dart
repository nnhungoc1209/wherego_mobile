import 'package:flutter/material.dart';
import 'package:where_go/models/traveler/review/restaurant/create_restaurant_review_model.dart';
import 'package:where_go/services/api/traveler/review/restaurant/restaurant_review_services.dart';
import 'package:where_go/services/api_status.dart';

class CreateRestaurantReviewViewModel extends ChangeNotifier {

  CreateRestaurantReviewModel createRestaurantReviewModel = CreateRestaurantReviewModel();

  String error = '';

  createRestaurantReview(String token, String email, int restaurantId, String comment, int rating) async {
    var result = await RestaurantReviewServices().createRestaurantReview(token, email, restaurantId, comment, rating);

    if (result is Failure) {
      error = result.errorResponse.toString();
      createRestaurantReviewModel = CreateRestaurantReviewModel();
    }

    if (result is Success) {
      error = '';
      createRestaurantReviewModel =
      result.response as CreateRestaurantReviewModel;
    }

    notifyListeners();
  }
}