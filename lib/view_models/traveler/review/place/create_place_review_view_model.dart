import 'package:flutter/material.dart';
import 'package:where_go/models/traveler/review/place/create_place_review_model.dart';
import 'package:where_go/services/api/traveler/review/place/place_review_services.dart';
import 'package:where_go/services/api_status.dart';

class CreatePlaceReviewViewModel extends ChangeNotifier {

  CreatePlaceReviewModel createPlaceReviewModel = CreatePlaceReviewModel();

  String error = '';

  createPlaceReview(String token, String email, int placeId, String comment, int rating) async {
    var result = await PlaceReviewServices().createPlaceReview(token, email, placeId, comment, rating);

    if (result is Failure) {
      error = result.errorResponse.toString();
      createPlaceReviewModel = CreatePlaceReviewModel();
    }

    if (result is Success) {
      error = '';
      createPlaceReviewModel =
      result.response as CreatePlaceReviewModel;
    }
    notifyListeners();
  }
}