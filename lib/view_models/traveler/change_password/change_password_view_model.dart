import 'package:flutter/material.dart';
import 'package:where_go/models/traveler/change_password/change_password_model.dart';
import 'package:where_go/services/api/traveler/traveler_services.dart';
import 'package:where_go/services/api_status.dart';

class ChangePasswordViewModel extends ChangeNotifier {

  ChangePasswordModel changePasswordModel = ChangePasswordModel();

  String error = '';

  changePassword(String token, String email, String oldPassword, String newPassword) async {
    var result = await TravelerServices().changePassword(token, email, oldPassword, newPassword);

    if (result is Failure) {
      error = result.errorResponse.toString();
      changePasswordModel = ChangePasswordModel();
    }

    if (result is Success) {
      error = '';
      changePasswordModel = result.response as ChangePasswordModel;
    }

    notifyListeners();
  }
}