import 'dart:io';

import 'package:flutter/material.dart';
import 'package:where_go/models/traveler/update_information/update_information_model.dart';
import 'package:where_go/services/api/traveler/traveler_services.dart';
import 'package:where_go/services/api_status.dart';

class UpdateInformationViewModel extends ChangeNotifier {

  UpdateInformationModel updateInformationModel = UpdateInformationModel();

  String error = '';

  updateInformation(String token, String username, String name, String tel, DateTime dOB, File? avatar) async {

    var result = await TravelerServices().updateInformation(token, username, name, tel, dOB, avatar);

    if(result is Failure) {
      error = result.errorResponse.toString();
      updateInformationModel = UpdateInformationModel();
    }

    if(result is Success) {
      error = '';
      updateInformationModel = result.response as UpdateInformationModel;
    }

    notifyListeners();
  }

}