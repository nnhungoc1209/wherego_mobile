import 'package:flutter/material.dart';
import 'package:where_go/models/traveler/booking/create_booking_model.dart';
import 'package:where_go/services/api/traveler/traveler_services.dart';
import 'package:where_go/services/api_status.dart';

class BookingHotelViewModel extends ChangeNotifier {

  CreateBookingModel bookingModel = CreateBookingModel();

  String error = '';

  bookingHotel(String token, String travelerEmail, int hotelId, DateTime bookingDate, int numberOfPeople, int price, DateTime checkInDate, DateTime checkOutDate) async {
    var result = await TravelerServices().bookingHotel(token, travelerEmail, hotelId, bookingDate, numberOfPeople, price, checkInDate, checkOutDate);

    if(result is Failure) {
      error = result.errorResponse.toString();
      bookingModel = CreateBookingModel();
    }

    if(result is Success) {
      error = '';
      bookingModel = result.response as CreateBookingModel;
    }

    notifyListeners();
  }
}