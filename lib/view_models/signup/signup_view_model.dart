import 'dart:io';

import 'package:flutter/material.dart';
import 'package:where_go/models/signup/signup_model.dart';
import 'package:where_go/services/api/account/account_services.dart';
import 'package:where_go/services/api_status.dart';

class SignupViewModel extends ChangeNotifier {

  SignupModel signupModel = SignupModel();

  String error = '';

  signup(String email, String password, String username, String name, String tel, File? avatar, DateTime dOB) async {


    var result = await AccountServices().signup(email, password, username, name, tel, avatar, dOB);

    if(result is Failure) {
      error = result.errorResponse.toString();
      signupModel = SignupModel();
    }

    if(result is Success) {
      error = '';
      signupModel = result.response as SignupModel;
    }

    notifyListeners();
  }
}