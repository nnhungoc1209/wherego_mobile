import 'package:flutter/material.dart';
import 'package:where_go/models/login/login_model.dart';
import 'package:where_go/services/api/account/account_services.dart';
import 'package:where_go/services/api_status.dart';

class LoginViewModel extends ChangeNotifier {

  LoginModel loginModel = LoginModel();

  String error = '';

  login(String email, String password) async {
    var result = await AccountServices().login(email, password);

    if(result is Failure) {
      error = result.errorResponse.toString();
      loginModel = LoginModel();
    }

    if(result is Success) {
      error = '';
      loginModel = result.response as LoginModel;
    }

    notifyListeners();
  }
}