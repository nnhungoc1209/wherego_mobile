import 'package:flutter/cupertino.dart';
import 'package:where_go/models/article/article_detail_model.dart';
import 'package:where_go/services/api/article/article_services.dart';
import 'package:where_go/services/api_status.dart';

class GetArticleDetailViewModel extends ChangeNotifier {
  ArticlesDetailModel articlesDetailModel = ArticlesDetailModel();

  String error = '';

  getArticleDetail(int id) async {

    var result = await ArticleServices().getArticleDetail(id);

    if (result is Failure) {
      error = result.errorResponse.toString();
      articlesDetailModel = ArticlesDetailModel();
    }

    if (result is Success) {
      error = '';
      articlesDetailModel = result.response as ArticlesDetailModel;
    }

    notifyListeners();
  }
}
