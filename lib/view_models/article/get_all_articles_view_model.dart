 import 'package:flutter/cupertino.dart';
import 'package:where_go/services/api/article/article_services.dart';
import 'package:where_go/services/api_status.dart';

class GetAllArticlesViewModel extends ChangeNotifier {
  List<dynamic> articleModelList = [];

  String error = '';

  getAllArticles() async {
    var result = await ArticleServices().getAllArticle();

    if (result is Failure) {
      error = result.errorResponse.toString();
      articleModelList = [];
    }

    if (result is Success) {
      error = '';
      articleModelList = result.response as List<dynamic>;
    }

    notifyListeners();
  }
}