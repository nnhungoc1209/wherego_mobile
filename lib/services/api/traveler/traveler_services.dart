import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:where_go/models/traveler/booking/create_booking_model.dart';
import 'package:where_go/models/traveler/change_password/change_password_model.dart';
import 'package:where_go/models/traveler/profile/detail_travler_model.dart';
import 'package:where_go/models/traveler/update_information/update_information_model.dart';
import 'package:where_go/services/api_status.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';

class TravelerServices {

  Future<Object> getTravelerDetail(String username, String token) async {
    DetailTravelerModel detailTravelerModel = DetailTravelerModel();
    print('Service token: $token');

    try {
      Dio dio = Dio();
      dio.options.headers['Authorization'] = 'Bearer $token';
      dio.options.headers['Content-Type'] = 'application/json';

      var response = await dio.get(AppConstantApi.baseUrl + AppConstantApi.detailTravler + username);

      if (response.data != null && response.statusCode == 200) {
        detailTravelerModel = DetailTravelerModel.fromJson(response.data);
      }

      return Success(code: response.statusCode, response: detailTravelerModel);
    } on DioError catch (e) {
      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']
      );
    }
  }

  Future<Object> changePassword(String token, String email, String oldPassword, String newPassword) async {
    ChangePasswordModel changePasswordModel = ChangePasswordModel();

    var params = {
      'email': email,
      'oldPassword': oldPassword,
      'newPassword': newPassword,
    };

    try {
      Dio dio = Dio();
      dio.options.headers['Authorization'] = 'Bearer $token';
      dio.options.headers['Content-Type'] = 'application/json';

      var response = await dio.put(AppConstantApi.baseUrl + AppConstantApi.changePassword, data: jsonEncode(params));

      if (response.data != null && response.statusCode == 200) {
        changePasswordModel = ChangePasswordModel.fromJson(response.data);
      }

      return Success(
          code: response.statusCode,
          response: changePasswordModel
      );

    } on DioError catch (e) {

      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']
      );
    }
  }

  Future<Object> bookingHotel(String token, String travelerEmail, int hotelId, DateTime bookingDate, int numberOfPeople, int price, DateTime checkInDate, DateTime checkOutDate) async {
    CreateBookingModel bookingModel = CreateBookingModel();

    var params = {
      'travelerEmail': travelerEmail,
      'hotelId': hotelId,
      'bookingDate': bookingDate.toString().substring(0, 10),
      'numberOfPeople': numberOfPeople,
      'price': price,
      'checkInDate': checkInDate.toString().substring(0, 10),
      'checkOutDate': checkOutDate.toString().substring(0, 10),
    };

    try{
      Dio dio = Dio();
      dio.options.headers['Authorization'] = 'Bearer $token';
      dio.options.headers['Content-Type'] = 'application/json';

      var response = await dio.post(AppConstantApi.baseUrl + AppConstantApi.booking, data: params);

      if (response.data != null && response.statusCode == 200) {
        bookingModel = CreateBookingModel.fromJson(response.data);
      }

      return Success(
          code: response.statusCode,
          response: bookingModel
      );

    } on DioError catch (e) {
      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']
      );
    }
  }

  Future<Object> updateInformation(String token, String username, String name, String tel, DateTime dOB, File? avatar) async {
    UpdateInformationModel updateInformationModel = UpdateInformationModel();
    MultipartFile? multipartFile;

    DateTime dayOfBirth = DateFormat('yyyy-MM-dd').parse(dOB.toString());

    if (avatar != null) {
      multipartFile = await MultipartFile.fromFile(avatar.path, filename: avatar.path.split("/").last);
    }

    FormData formData = FormData.fromMap({
      'email': username,
      'name': name,
      'tel': tel,
      'avatar': multipartFile,
      'dob': dayOfBirth,
    });

    try{
      Dio dio = Dio();
      dio.options.headers['Authorization'] = 'Bearer $token';
      dio.options.headers['Content-Type'] = 'application/json';

      var response = await dio.put(AppConstantApi.baseUrl + AppConstantApi.detailTravler + username, data: formData);


      if (response.data != null && response.statusCode == 200) {
        updateInformationModel = UpdateInformationModel.fromJson(response.data);
      }

      return Success(
          code: response.statusCode,
          response: updateInformationModel
      );

    } on DioError catch (e) {
      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']
      );
    }

  }
}