import 'package:dio/dio.dart';
import 'package:where_go/models/traveler/review/place/create_place_review_model.dart';
import 'package:where_go/services/api_status.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';

class PlaceReviewServices {

  Future<Object> createPlaceReview(String token, String email, int placeId, String comment, int rating) async {
    CreatePlaceReviewModel createPlaceReviewModel = CreatePlaceReviewModel();

    var params = {
      'travelerEmail': email,
      'placeId': placeId,
      'comment': comment,
      'rating': rating,
    };

    try {
      Dio dio = Dio();
      dio.options.headers['Authorization'] = 'Bearer $token';
      dio.options.headers['Content-Type'] = 'application/json';

      var response = await dio.post(AppConstantApi.baseUrl + AppConstantApi.placeReview  + placeId.toString() + '/review', data: params);

      if (response.data != null && response.statusCode == 201) {
        createPlaceReviewModel = CreatePlaceReviewModel.fromJson(response.data);
      }

      return Success(
          code: response.statusCode,
          response: createPlaceReviewModel
      );

    } on DioError catch (e) {

      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']
      );
    }
  }

}