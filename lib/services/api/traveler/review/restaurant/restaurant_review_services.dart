import 'package:dio/dio.dart';
import 'package:where_go/models/traveler/review/restaurant/create_restaurant_review_model.dart';
import 'package:where_go/services/api_status.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';

class RestaurantReviewServices {

  Future<Object> createRestaurantReview(String token, String email, int restaurantId, String comment, int rating) async {
    CreateRestaurantReviewModel createRestaurantReviewModel = CreateRestaurantReviewModel();

    var params = {
      'travelerEmail': email,
      'restaurantId': restaurantId,
      'comment': comment,
      'rating': rating,
    };

    try {
      Dio dio = Dio();
      dio.options.headers['Authorization'] = 'Bearer $token';
      dio.options.headers['Content-Type'] = 'application/json';

      var response = await dio.post(AppConstantApi.baseUrl + AppConstantApi.restaurantReview + restaurantId.toString() + '/review', data: params);

      if (response.data != null && response.statusCode == 201) {
        createRestaurantReviewModel = CreateRestaurantReviewModel.fromJson(response.data);
      }

      return Success(
          code: response.statusCode,
          response: createRestaurantReviewModel
      );

    } on DioError catch (e) {

      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']
      );
    }
  }

}