import 'package:dio/dio.dart';
import 'package:where_go/models/traveler/review/hotel/create_hotel_review_model.dart';
import 'package:where_go/services/api_status.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';

class HotelReviewServices {

  Future<Object> createHotelReview(String token, String email, int hotelId, String comment, int rating) async {
    CreateHotelReviewModel createHotelReviewModel = CreateHotelReviewModel();

    var params = {
      'travelerEmail': email,
      'hotelId': hotelId,
      'comment': comment,
      'rating': rating,
    };

    try {
      Dio dio = Dio();
      dio.options.headers['Authorization'] = 'Bearer $token';
      dio.options.headers['Content-Type'] = 'application/json';

      var response = await dio.post(AppConstantApi.baseUrl + AppConstantApi.hotelReview + hotelId.toString() + '/review', data: params);

      if (response.data != null && response.statusCode == 201) {
        createHotelReviewModel = CreateHotelReviewModel.fromJson(response.data);
      }

      return Success(
        code: response.statusCode,
        response: createHotelReviewModel
      );

    } on DioError catch (e) {

      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']
      );
    }
  }

}