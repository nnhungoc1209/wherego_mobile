import 'package:dio/dio.dart';
import 'package:where_go/models/random/random_hotel_model.dart';
import 'package:where_go/models/random/random_restaurant_model.dart';
import 'package:where_go/models/random/random_place_model.dart';
import 'package:where_go/services/api_status.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';

class RandomServices {
  Future<Object> randomHotel() async {
    List<dynamic> randomHotelModelList = [];

    try {
      Dio dio = Dio();
      var response = await dio.get(AppConstantApi.baseUrl + AppConstantApi.randomHotel);

      if (response.data != null && response.statusCode == 200) {
        randomHotelModelList = response.data
            .map((item) => RandomHotelModel.fromJson(item))
            .toList();
      }

      return Success(code: response.statusCode, response: randomHotelModelList);
    } on DioError catch (e) {
      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']);
    }
  }

  Future<Object> randomPlace() async {
    List<dynamic> randomPlaceModelList = [];

    try {
      Dio dio = Dio();
      var response = await dio.get(AppConstantApi.baseUrl + AppConstantApi.randomPlace);

      if (response.data != null && response.statusCode == 200) {
        randomPlaceModelList = response.data
          .map((item) => RandomPlaceModel.fromJson(item))
          .toList();
      }

      return Success(code: response.statusCode, response: randomPlaceModelList);
    } on DioError catch (e) {
      return Failure(
        code: e.response!.statusCode,
        errorResponse: e.response!.data['message']);
    }
  }

  Future<Object> randomRestaurant() async {
    List<dynamic> randomRestaurantModelList = [];

    try {
      Dio dio = Dio();
      var response = await dio.get(AppConstantApi.baseUrl + AppConstantApi.randomRestaurant);
      if (response.data != null && response.statusCode == 200) {
        randomRestaurantModelList = response.data
          .map((item) => RandomRestaurantModel.fromJson(item))
          .toList();
      }

      return Success(
        code: response.statusCode,
        response: randomRestaurantModelList
      );
    } on DioError catch (e) {
      return Failure(
        code: e.response!.statusCode,
        errorResponse: e.response!.data['message']);
    }
  }
}
