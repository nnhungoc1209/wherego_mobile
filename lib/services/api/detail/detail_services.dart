import 'package:dio/dio.dart';
import 'package:where_go/models/detail/hotel/detail_hotel_model.dart';
import 'package:where_go/models/detail/place/detail_place_model.dart';
import 'package:where_go/models/detail/restaurant/detail_restaurant_model.dart';
import 'package:where_go/services/api_status.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';

class DetailServices {
  Future<Object> detailHotel(int id) async {
    DetailHotelModel detailHotelModel = DetailHotelModel();

    try {
      Dio dio = Dio();
      var response = await dio.get(
          AppConstantApi.baseUrl + AppConstantApi.detailHotel + id.toString());

      if (response.data != null && response.statusCode == 200) {
        detailHotelModel = DetailHotelModel.fromJson(response.data);
      }

      return Success(code: response.statusCode, response: detailHotelModel);
    } on DioError catch (e) {
      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']);
    }
  }

  Future<Object> detailPlace(int id) async {
    DetailPlaceModel detailPlaceModel = DetailPlaceModel();

    try {
      Dio dio = Dio();
      var response = await dio.get(
          AppConstantApi.baseUrl + AppConstantApi.detailPlace + id.toString());

      if (response.data != null && response.statusCode == 200) {
        detailPlaceModel = DetailPlaceModel.fromJson(response.data);
      }

      return Success(code: response.statusCode, response: detailPlaceModel);
    } on DioError catch (e) {
      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']);
    }
  }

  Future<Object> detailRestaurant(int id) async {
    DetailRestaurantModel detailRestaurantModel = DetailRestaurantModel();

    try {
      Dio dio = Dio();
      var response = await dio.get(AppConstantApi.baseUrl +
          AppConstantApi.detailRestaurant +
          id.toString());

      if (response.data != null && response.statusCode == 200) {
        detailRestaurantModel = DetailRestaurantModel.fromJson(response.data);
      }

      return Success(
          code: response.statusCode, response: detailRestaurantModel);
    } on DioError catch (e) {
      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']);
    }
  }
}
