import 'dart:io';

import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:where_go/models/login/login_model.dart';
import 'package:where_go/models/signup/signup_model.dart';
import 'package:where_go/services/api_status.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';
import 'package:where_go/shared/app_constant/app_constant_shared_preferences.dart';

class AccountServices {
  Future<Object> login(String email, String password) async {
    var loginModel = LoginModel();

    var params = {
      'email': email,
      'password': password
    };

    try{
      Dio dio = Dio();
      var response = await dio.post(AppConstantApi.baseUrl + AppConstantApi.loginApiUrl, data: params);

      if(response.data != null && response.statusCode == 200) {
        String? jwtToken = response.data['token'];
        String username = response.data['username'];
        String token = (jwtToken == null) ? '' : jwtToken;

        var dateFormatter = DateFormat('yyyy-MM-dd HH:mm:ss');
        String currentDate = dateFormatter.format(DateTime.now());
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString(AppConstantSharedPreferences.sessionEmail, email);
        await prefs.setString(AppConstantSharedPreferences.sessionPassword, password);
        await prefs.setString(AppConstantSharedPreferences.sessionUsername, username);
        await prefs.setString(AppConstantSharedPreferences.sessionJWTToken, token);
        await prefs.setString(AppConstantSharedPreferences.sessionTimeLogin, currentDate);
        await prefs.setBool(AppConstantSharedPreferences.isLogin, true);

        loginModel = LoginModel.fromJson(response.data);
      }

      return Success(
        code: response.statusCode,
        response: loginModel
      );

    } on DioError catch (e) {
      return Failure(
        code: e.response!.statusCode,
        errorResponse: e.response!.data['message']
      );
    }
  }

  Future<Object> signup(String email, String password, String username, String name, String tel, File? avatar, DateTime dOB) async {
    var signupModel = SignupModel();
    MultipartFile? multipartFile;

    DateTime dayOfBirth = DateFormat('yyyy-MM-dd').parse(dOB.toString());

    if (avatar != null) {
      multipartFile = await MultipartFile.fromFile(avatar.path, filename: avatar.path.split("/").last);
    }

    FormData formData = FormData.fromMap({
      'email': email,
      'password': password,
      'username': username,
      'name': name,
      'tel': tel,
      'avatar': multipartFile,
      'dob': dayOfBirth,
    });

    try{
      Dio dio = Dio();
      //Dio dio = Dio(BaseOptions(responseType: ResponseType.plain));

      //var response = await dio.post('http://192.168.1.14:8080/api/signup', data: formData);
      var response = await dio.post(AppConstantApi.baseUrl + AppConstantApi.signupApiUrl, data: formData);

      if(response.data != null && response.statusCode == 201) {
        signupModel = SignupModel.fromJson(response.data);
      }

      return Success(
        code: response.statusCode,
        response: signupModel
      );

    } on DioError catch (e) {
      return Failure(code: e.response!.statusCode, errorResponse: e.response!.data['message']);
    }
  }
}