import 'package:dio/dio.dart';
import 'package:where_go/models/article/article_detail_model.dart';
import 'package:where_go/models/article/get_all_article_model.dart';
import 'package:where_go/services/api_status.dart';
import 'package:where_go/shared/app_constant/app_constant_api.dart';

class ArticleServices {
  Future<Object> getAllArticle() async {
    List<dynamic> articleModelList = [];

    try {
      Dio dio = Dio();
      var response = await dio.get(AppConstantApi.baseUrl + AppConstantApi.getAllArticles);

      if (response.data != null && response.statusCode == 200) {
        articleModelList = response.data
            .map((item) => GetAllArticlesModel.fromJson(item))
            .toList();
      }

      return Success(
          code: response.statusCode,
          response: articleModelList
      );
    } on DioError catch (e) {
      return Failure(
        code: e.response!.statusCode,
        errorResponse: e.response!.data['message']
      );
    }
  }

  Future<Object> getArticleDetail(int id) async {
    ArticlesDetailModel articlesDetailModel = ArticlesDetailModel();

    try {
      Dio dio = Dio();
      var response = await dio.get(AppConstantApi.baseUrl + AppConstantApi.detailArticle + id.toString());

      if (response.data != null && response.statusCode == 200) {
        articlesDetailModel = ArticlesDetailModel.fromJson(response.data);
      }

      return Success(code: response.statusCode, response: articlesDetailModel);
    } on DioError catch (e) {
      return Failure(
          code: e.response!.statusCode,
          errorResponse: e.response!.data['message']);
    }
  }
}