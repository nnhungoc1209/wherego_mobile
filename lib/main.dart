import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:where_go/modules/account/account_page.dart';
import 'package:where_go/modules/traveler/profile/profile_page.dart';
import 'package:where_go/modules/article/articles_page.dart';
import 'package:where_go/modules/hotel/hotels_page.dart';
import 'package:where_go/modules/login/login_page.dart';
import 'package:where_go/modules/main_page/main_page.dart';
import 'package:where_go/modules/place/places_page.dart';
import 'package:where_go/modules/restaurant/restaurants_page.dart';
import 'package:where_go/modules/search/search_page.dart';
import 'package:where_go/modules/search/search_with_input_page.dart';
import 'package:where_go/modules/signup/signup_page.dart';
import 'package:where_go/provider/provider_custom.dart';
import 'package:where_go/shared/app_colors.dart';
import 'package:where_go/shared/app_constant/app_constant_route.dart';

import 'modules/traveler/booking/booking_page.dart';
import 'modules/traveler/change_password/change_password_page.dart';
import 'modules/traveler/favorite/favorite_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: ProviderCustom().getListMultiProvider(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'WhereGo',
        theme: ThemeData(
          primaryColor: AppColors.bluePrimary,
        ),
        home: const LoginPage(),
        routes: {
          AppConstantRoute.signUp: (context) => const SignUpPage(),
          AppConstantRoute.login: (context) => const LoginPage(),
          AppConstantRoute.articles: (context) => const ArticlesPage(),
          AppConstantRoute.hotels: (context) => const HotelsPage(),
          AppConstantRoute.restaurants: (context) => const RestaurantsPage(),
          AppConstantRoute.places: (context) => const PlacesPage(),
          AppConstantRoute.mainpage: (context) => const MainPage(),
          AppConstantRoute.search: (context) => const SearchPage(),
          AppConstantRoute.account: (context) => const AccountPage(),
          AppConstantRoute.searchWithInput: (context) => const SearchWithInputPage(),
          AppConstantRoute.profile: (context) => const ProfilePage(),
          AppConstantRoute.booking: (context) => const BookingPage(),
          AppConstantRoute.changePassword: (context) => const ChangePasswordPage(),
          AppConstantRoute.favorite: (context) => const FavoritePage(),
        },
      ),
    );
  }
}





